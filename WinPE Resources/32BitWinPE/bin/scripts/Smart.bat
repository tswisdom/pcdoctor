:Smart
azcopy /Y /@:"x:\Windows\System32\bin\scripts\azcopyanswer.txt" /pattern:"!ID!-SMART.csv" > nul
if !errorlevel! NEQ 0 (
	color c0
	echo PC Doctor failed to upload SMART Results.
	echo Press any key to retry upload. && pause > nul
	goto Smart
	color
	cls
)
:EOF