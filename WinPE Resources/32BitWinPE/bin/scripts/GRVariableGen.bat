SETLOCAL ENABLEDELAYEDEXPANSION
for /f "tokens=1,2 delims=:" %%a in (x:\Windows\System32\bin\Logs\LocalVariables.txt) do set %%a=%%b
echo.
echo The following values will be uploaded to the GRID.
echo If any of these values are incorrect or missing, please make any necessary updates manually.
if !Fgrade!==Functional if !Incomplete!==True set Fgrade=Functional-Incomplete
if exist x:\Windows\System32\bin\Logs\notes.txt (
	set notes=
	for /F "delims=" %%i in (x:\Windows\System32\bin\Logs\notes.txt) do set notes=!notes! %%i
)
echo.
echo Asset ID: !ID!
if defined TechnicianUserName echo Technician ID: !TechnicianUserName!
if defined AssetLocation echo Asset Location: !Location!
if defined Istatus echo Inventory Status: !Istatus!
if defined aux1 echo Auxiliary Field 1: !aux1!
if defined aux2 echo Auxiliary Field 2: !aux2!
if defined notes echo Notes: !notes!
if defined Cgrade echo Cosmetic Grade: !Cgrade!
if defined Fgrade echo Functional Grade: !Fgrade!
if defined Webcam echo Webcam: !Webcam!
if defined KeyboardLanguage echo Keyboard Language: !KeyboardLanguage!
if defined Ssize echo Screen Size: !Ssize!
if defined Adapter echo AC Adapter: !Adapter!
if defined OSType echo OS Type: !OSType!

for /f "tokens=1,2 delims=:" %%a in (x:\Windows\System32\bin\scripts\IstatusCodes.csv) do if /i %Istatus%==%%a set IstatusCode=%%b
for /f "tokens=1,2 delims=:" %%a in (x:\Windows\System32\bin\scripts\FgradeCodes.csv) do if /i %Fgrade%==%%a set FgradeCode=%%b
for /f "tokens=1,2 delims=:" %%a in (x:\Windows\System32\bin\scripts\CgradeCodes.csv) do if /i %Cgrade%==%%a set CgradeCode=%%b

if defined notes set comments=!notes!

echo TechnicianUserName: !TechnicianUserName!>x:\Windows\System32\bin\Logs\!ID!-GRVariables.yaml
echo AssetLocation: !Location!>>x:\Windows\System32\bin\Logs\!ID!-GRVariables.yaml
echo AssetStatus: !IstatusCode!>>x:\Windows\System32\bin\Logs\!ID!-GRVariables.yaml
if defined Aux1 (
	echo AuxField1: !Aux1!>>x:\Windows\System32\bin\Logs\!ID!-GRVariables.yaml
)
if defined Aux2 (
	echo AuxField2: !Aux2!>>x:\Windows\System32\bin\Logs\!ID!-GRVariables.yaml
)
if defined notes (
	echo Notes: !notes!>>x:\Windows\System32\bin\Logs\!ID!-GRVariables.yaml
)
if defined comments (
	echo Comments: !comments!>>x:\Windows\System32\bin\Logs\!ID!-GRVariables.yaml
)
echo CosmeticGrade: !CgradeCode!>>x:\Windows\System32\bin\Logs\!ID!-GRVariables.yaml
echo FunctionalGrade: !FgradeCode!>>x:\Windows\System32\bin\Logs\!ID!-GRVariables.yaml
if defined Webcam echo Webcam: !Webcam!>>x:\Windows\System32\bin\Logs\!ID!-GRVariables.yaml
if defined KeyboardLanguage echo KeyboardLanguage: !KeyboardLanguage!>>x:\Windows\System32\bin\Logs\!ID!-GRVariables.yaml
if defined Ssize echo ScreenSize: !Ssize!>>x:\Windows\System32\bin\Logs\!ID!-GRVariables.yaml
if defined Adapter echo ACAdapter: !Adapter!>>x:\Windows\System32\bin\Logs\!ID!-GRVariables.yaml
if defined OSType echo OSType: !OSType!>>x:\Windows\System32\bin\Logs\!ID!-GRVariables.yaml

timeout 10 > nul
azcopy /Y /@:"x:\Windows\System32\bin\scripts\azcopyanswer.txt"  /pattern:"!ID!-GRVariables.yaml">nul
if !errorlevel! NEQ 0 (
	x:\Windows\System32\JPEGView.exe x:\Windows\System32\Images\Error3.tif
	color c0
	echo PC Doctor failed to upload GRID values.
	echo Press any key to retry upload.&&pause>nul
	call x:\Windows\System32\bin\scripts\GRVariables.bat
	color
	cls
)

:EOF