:Diag
azcopy /Y /@:"x:\Windows\System32\bin\scripts\azcopyanswer.txt" /pattern:"%ID%-DIAG.csv" > nul
if !errorlevel! NEQ 0 (
	color c0
	echo PC Doctor failed to upload Diagnostic Results.
	choice /m "Would you like to retry upload?"
	if !errorlevel!==1 goto Diag
	color
	cls
)
:EOF