:SysInfo
pcd sysinfo -s %IP% -f>x:\Windows\System32\bin\Logs\!ID!-SYSINFO.yaml
if !errorlevel! NEQ 0 (
	color c0
	echo PC Doctor failed to generate System Information.
	echo Press any key to retry.&&pause>nul
	goto SysInfo
	color
	cls
)
for /f "tokens=2 delims=:" %%f in ('find /I /C "License Authentication Error" x:\Windows\System32\bin\Logs\!ID!-SYSINFO.yaml') do if %%f==1 (
	color c0
	echo PC Doctor failed to generate System Information.
	echo Press any key to retry.&&pause>nul
	goto SysInfo
	color
	cls
)
	
:EOF