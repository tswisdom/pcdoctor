:SysInfo
azcopy /Y /@:"x:\Windows\System32\bin\scripts\azcopyanswer.txt" /pattern:"!ID!-SYSINFO.yaml" > nul
if !errorlevel! NEQ 0 (
	color c0
	echo PC Doctor failed to upload System Information.
	echo Press any key to retry upload. && pause > nul
	goto SysInfo
	color
	cls
) else (
	set sysinfo=Pass
)
:EOF