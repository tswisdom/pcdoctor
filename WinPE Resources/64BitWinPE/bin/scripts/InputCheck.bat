:InputLoop
if exist x:\Windows\System32\bin\Logs\input.txt (
	for /f "tokens=1,2 delims=:" %%a in (x:\Windows\System32\bin\Logs\input.txt) do set %%a=%%b
	set Input=True&&echo Input:!Input!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
	cls
) else (
	color c0
	echo Please finish providing inputs in the second terminal window
	echo and press any key to resume workflow. [Alt+Tab to change windows]
	pause>nul
	color
	cls
	goto InputLoop
)
:EOF