:: Modified 05/30/17 to automatically apply Windows 10 Pro 64 bit if no entry is received. Replaced Pause function with Timeout function in WinXXX loops.

@ECHO OFF
CLS
Z:

:Imaging
CLS
ECHO.
ECHO Deploy or Capture a Windows 64 bit image. If you need to deploy 32-bit Windows then please boot using the 32-bit USB WinPE
Echo.
ECHO Which version of Windows do you want to install?
ECHO [1] Windows 10 Professional (64-bit)
ECHO [2] Global Resale Image (64-bit)
ECHO [3] Windows 8.1 Professional (64-bit)
ECHO [4] Windows 8.1 (64-bit)
ECHO [5] Windows 7 Professional (64-bit)
ECHO [6] Windows 7 Home Premium (64-bit)
ECHO [7] Model Specific Image (64-bit)
ECHO.
ECHO Or...
ECHO.
ECHO [C] Capture a Model Specific Image (64-bit)
ECHO [X] Exit to WinPE
ECHO [H] for HELP
ECHO.
ECHO.
choice /c 1234567cxh /n /t 10 /d 1 /m "Please make your selection: "
set Windv=%errorlevel%

IF %Windv% EQU 1 GOTO Win10Pro
IF %Windv% EQU 2 GOTO Win10Home
IF %Windv% EQU 3 GOTO Win81Pro
IF %Windv% EQU 4 GOTO Win81core
IF %Windv% EQU 5 GOTO Win7Pro
IF %Windv% EQU 6 GOTO Win7Home
IF %Windv% EQU 7 GOTO ModSpec
IF %Windv% EQU 8 GOTO Capture
IF %Windv% EQU 9 GOTO Console
IF %Windv% EQU 10 GOTO Usage
GOTO Imaging
REM =========================================================================================================
:Win10Pro
SET OSV=Win10Pro
SET OSARCH=64-bit
SET INDEX7=1
SET DeployDrivers=YES
SET OSType=MAR CU Windows 10 Pro, 64 Bit&&echo OSType:!OSType!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
REM Resource: Microsoft OEM ADK Fest Sample scripts, Athens, Greece 2015
ECHO About to install the following image;
DISM /get-wiminfo /wimfile:"Z:\Images\Win10\64-bit\install.wim" /Index:%INDEX7%
timeout 5
Call Z:\Scripts\Win10\Walkthrough-Deploy-Image.bat Z:\Images\Win10\64-bit\install.wim
GOTO Console
REM =========================================================================================================
:Win10Home
SET OSV=Win10Home
SET OSARCH=64-bit
SET INDEX7=1
SET DeployDrivers=YES
ECHO About to install the following image;
DISM /get-wiminfo /wimfile:"Z:\Images\Win10\64-bit\custom.wim" /Index:%INDEX7%
timeout 5
Call Z:\Scripts\Win10\Walkthrough-Deploy-Image.bat Z:\Images\Win10\64-bit\custom.wim
GOTO Console
REM =========================================================================================================
:Win81Pro
SET OSV=Win8Pro
SET OSARCH=64-bit
SET INDEX7=1
SET DeployDrivers=YES
REM https://technet.microsoft.com/en-us/library/hh825204.aspx

ECHO About to install the following image;
DISM /get-wiminfo /wimfile:"Z:\Images\Win8\64-bit\install.wim" /Index:%INDEX7%
timeout 5
Call Z:\Scripts\Win8\Walkthrough-Deploy-Image.bat Z:\Images\Win8\64-bit\install.wim
GOTO Console
REM =========================================================================================================
:Win81core
SET OSV=Win8Core
SET OSARCH=64-bit
SET INDEX7=2
SET DeployDrivers=YES
ECHO About to install the following image;
DISM /get-wiminfo /wimfile:"Z:\Images\Win8\64-bit\install.wim" /Index:%INDEX7%
timeout 5
Call Z:\Scripts\Win8\Walkthrough-Deploy-Image.bat Z:\Images\Win8\64-bit\install.wim
GOTO Console
REM =========================================================================================================
:Win7Pro
SET OSV=Win7Pro
SET OSARCH=64-bit
SET INDEX7=3
SET DeployDrivers=YES
ECHO About to install the following image;
DISM /get-wiminfo /wimfile:"Z:\Images\Win7\64-bit\install.wim" /Index:%INDEX7%
timeout 5
Call Z:\Scripts\Win7\Walkthrough-Deploy-Image.bat Z:\Images\Win7\64-bit\install.wim
GOTO Console
REM =========================================================================================================
:Win7Home
SET OSV=Win7HomePremium
SET OSARCH=64-bit
SET INDEX7=2
SET DeployDrivers=YES
ECHO About to install the following image;
DISM /get-wiminfo /wimfile:"Z:\Images\Win7\64-bit\install.wim" /Index:%INDEX7%
timeout 5
Call Z:\Scripts\Win7\Walkthrough-Deploy-Image.bat Z:\Images\Win7\64-bit\install.wim
GOTO Console
REM =========================================================================================================

:ModSpec
CALL Z:\Scripts\modelspecific-deploy-menu-64.cmd
GOTO Console
REM =========================================================================================================

:Capture

ECHO About to Capture the image. Note: Image must be Syspreped before capture;
CALL Z:\Scripts\Capture\Walkthrough-Capture-Image.bat
GOTO Console

:Console
ECHO.
ECHO Do you want to go back to the main menu or shutdown?
ECHO [A] to return to the main menu
ECHO [Q] to SHUTDOWN the PC
ECHO [R] to REBOOT the PC
ECHO [W] To remove Windows image.
ECHO [C] command prompt
SET /P loop=Enter your selection: 
IF %loop% EQU w (
	diskpart /s "x:\Windows\System32\bin\Scripts\RemoveImage.txt"
	echo The System Image has been removed
	timeout 3 > nul
	goto Console
)
IF %loop% EQU W (
	diskpart /s "x:\Windows\System32\bin\Scripts\RemoveImage.txt"
	echo The System Image has been removed
	timeout 3 > nul
	goto Console
)
IF %loop% EQU A GOTO Imaging
IF %loop% EQU a GOTO Imaging
IF %loop% EQU Q WPEUTIL SHUTDOWN
IF %loop% EQU q WPEUTIL SHUTDOWN
IF %loop% EQU R WPEUTIL REBOOT
IF %loop% EQU r WPEUTIL REBOOT
IF %loop% EQU C GOTO EOF
IF %loop% EQU c GOTO EOF
GOTO Imaging
REM =========================================================================================================
:EOF