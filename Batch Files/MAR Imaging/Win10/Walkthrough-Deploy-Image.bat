REM Modified line 43 on 05/18/20 to remove user input. Added lines 98-112 to automatically reboot.


@cls
Set ErrorLevel=0
Set DRIVERSTATUS=OK
Set FIRMWARESTATUS=OK
Set DISKPARTSTATUS=OK
Set APPLYIMAGESTATUS=OK
Set SYSTEMBOOTSTATUS=OK
Set RECOVERYSTATUS=OK


REM **********************************************************************
REM Walkthrough-Deploy.bat
REM   Note: Run from the reference device in the WinPE environment
@echo.
@echo.
@if %1x==x echo Re-run this program with a path to a WIM file, example:
@if %1x==x echo Walkthrough-Deploy.bat D:\WindowsWithFrenchPlusApps.wim
@if %1x==x goto END
@echo.
@echo.
@echo   Detecting the firmware mode (BIOS or UEFI). 
@echo   Note: Some PCs may support both modes. 
@echo         Verify you're booted into the correct mode before proceeding: 
wpeutil UpdateBootInfo
for /f "tokens=2* delims=	 " %%A in ('reg query HKLM\System\CurrentControlSet\Control /v PEFirmwareType') DO SET Firmware=%%B
REM         Note: delims is a TAB followed by a space.
@echo.
if %Firmware%==0x1 echo Detected firmware mode: BIOS.
if %Firmware%==0x2 echo Detected firmware mode: UEFI.
@echo.
@echo   If this is correct, press a key to continue. 
@echo.
@echo   If this is NOT correct: Press Ctrl+C to exit this script.
@echo         Type exit to reboot the PC, and then
@echo         boot the USB key using the correct firmware mode.
if errorlevel 1 (
                 set errorlevel=0
                 Set FIRMWARESTATUS=FAILED
                )
TIMEOUT /T 5
@echo **********************************************************************
@echo.
@echo   Partition and format the disk
@echo   CAUTION: All the data on the primary disk will be DELETED.
REM pause
if %Firmware%==0x1 diskpart /s %~dp0CreatePartitions-BIOS.txt
if %Firmware%==0x2 diskpart /s %~dp0CreatePartitions-UEFI.txt
REM pause
if errorlevel 1 (
                 set errorlevel=0
                 Set DISKPARTSTATUS=FAILED
                )
@echo.
@echo.
@echo **********************************************************************
REM    Applying image
call %~dp0ApplyImage %1
if errorlevel 1 (
                 set errorlevel=0
                 Set APPLYIMAGESTATUS=FAILED
                )
@echo.
@echo **********************************************************************
@echo.
@echo    Hiding the recovery tools partition
@echo.
@echo.
if %Firmware%==0x1 diskpart /s %~dp0HideRecoveryPartitions-BIOS.txt
if %Firmware%==0x2 diskpart /s %~dp0HideRecoveryPartitions-UEFI.txt
if errorlevel 1 (
                 set errorlevel=0
                 Set DISKPARTSTATUS=FAILED
                )
@echo.
@echo.
@echo **********************************************************************
@echo  All done!
REM  Disconnect the USB drive from the reference device.
REM   Type exit to reboot.
@echo **********************************************************************
@echo.
@echo Check below for any FAILED Steps. If all steps are OK then device is ready to shutdown.
@echo.
@ECHO      Firmware detection: %FIRMWARESTATUS%
@echo.
@ECHO DISK Partition Creation: %DISKPARTSTATUS%
@echo.
@ECHO             Apply Image: %APPLYIMAGESTATUS%
@echo.
@ECHO          Adding Drivers: %DRIVERSTATUS%
@echo.
@ECHO     Setting System BOOT: %SYSTEMBOOTSTATUS%
@echo.
@ECHO   Recovery HDD Creation: %RECOVERYSTATUS%
REM Automatic reboot if installation was successful.

IF NOT %FIRMWARESTATUS%==FAILED (
	IF NOT %DISKPARTSTATUS%==FAILED (
		IF NOT %APPLYIMAGESTATUS%==FAILED (
			set Istatus=RETAIL READY
			call x:\Windows\System32\bin\scripts\GRVariableGen.bat
			IF NOT %DRIVERSTATUS%==FAILED (
				IF NOT %SYSTEMBOOTSTATUS%==FAILED (
					IF NOT %RECOVERYSTATUS%==FAILED (
						@ECHO Image has been successfully applied, rebooting automatically in 60 seconds.
						@timeout 60 > nul
						WPEUTIL REBOOT
					)
				)
			)
		)
	)
)
@echo. 
@pause
:END