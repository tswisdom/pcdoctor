REM Added lines 29-30 to implement SDI if deploy drivers failed 05/30/17


@Echo.

for /f "tokens=2 delims==" %%I in ('wmic computersystem get model /format:list') do set "SYSMODEL=%%I"
for /f "tokens=2 delims==" %%I in ('wmic computersystem get Manufacturer /format:list') do set "SYSMANUFACTURER=%%I"
REM %SYSMODEL%
REM %SYSMANUFACTURER%

for /f "tokens=3 delims=: " %%I in ('DISM /Image:W: /Get-Currentedition ^|find "Image Version:"') do set "OSVersion=%%I"
REM OSVersion: %OSVersion%
set WinRelease=Verison
Set OSVERSION4=%OSVERSION:~0,4%
if /I "%OSVERSION4%" == "10.0" SET "WINRELEASE=Win10"
if /I "%OSVERSION4%" == "6.3." SET "WINRELEASE=Win8"
if /I "%OSVERSION4%" == "6.1." SET "WINRELEASE=Win7"
if /I "%Winrelease%" =="Version" (
Echo Could not get OS Version. Drivers will not be deployed
Set DRIVERSTATUS=FAILED
GOTO END
)
REM Windows Version is %WinRelease% %OSARCH% 

@Echo Deploying Drivers for %SYSMANUFACTURER% %SYSMODEL% %WinRelease% %OSARCH%

IF EXIST "Z:\Drivers\%SYSMANUFACTURER%\%SYSMODEL%\%WinRelease%\%OSARCH%" (
	Dism /image:W:\ /Add-Driver /Driver:"Z:\Drivers\%SYSMANUFACTURER%\%SYSMODEL%\%WinRelease%\%OSARCH%" /Recurse
	) ELSE IF NOT EXIST "Z:\Drivers\%SYSMANUFACTURER%\%SYSMODEL%\%WinRelease%\%OSARCH%" (
		CALL z:\UpdateDrivers\SDI_ENG\SDI_x64_R1751 -cfg:unattended.cfg
		) ELSE (
		Color c0
		CLS
		@Echo.
		@Echo     浜様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様�
		@Echo     � *****   No Drivers found for this manufacturer/model                裁
		@Echo     � *****                                                       *****   裁
		@Echo     � *****   Have you tested device driver installation?         *****   裁
		@Echo     �                                                                     裁
		@Echo     � *****   THIS DEVICE MAY NOT HAVE ALL DRIVERS INSTALLED      *****   裁
		@Echo     �                                                                     裁
		@Echo     麺様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様鉱
		@Echo     � You can stop this script now by pressing Ctrl+C. If you stop now,   裁
		@Echo     � you will need to deploy the image to this device again.             裁
		@Echo     � Copyright Microsoft 2014                                            裁
		@Echo     藩様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様勺
		@Echo       栩栩栩栩栩栩栩栩栩栩栩栩栩栩栩栩栩栩栩栩栩栩栩栩栩栩栩栩栩栩栩栩栩栩栩
		@Echo.
		@Echo.
		@Echo.
		Set DRIVERSTATUS=FAILED
		@Echo Press any key to proceed
		pause>nul
		)

if errorlevel 1 (
                 set errorlevel=0
                 Set DRIVERSTATUS=FAILED
                )
:END