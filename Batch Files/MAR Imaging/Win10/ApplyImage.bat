REM == ApplyImage.bat ==
@echo.
@echo == These commands deploy a specified Windows
@echo    image file to the Windows partition, and configure
@echo    the system partition.
@echo.
REM   Usage:   ApplyImage WimFileName 
REM    Example: ApplyImage E:\WindowsWithFrench.wim ==
@echo.
@echo == Apply the image to the Windows partition ==
dism /Apply-Image /ImageFile:%1 /Index:%INDEX7% /ApplyDir:W:\
if errorlevel 1 (
                 set errorlevel=0
                 Set APPLYIMAGESTATUS=FAILED
                )
@echo.
@echo == Copy boot files to the System partition ==
W:\Windows\System32\bcdboot W:\Windows /s S:
if errorlevel 1 (
                 set errorlevel=0
                 Set SYSTEMBOOTSTATUS=FAILED
                )
@echo.
@echo == Copy the Windows RE image to the
@echo    Windows RE Tools partition ==
md R:\Recovery\WindowsRE
xcopy /h W:\Windows\System32\Recovery\Winre.wim R:\Recovery\WindowsRE\
REM del W:\Windows\System32\Recovery\Winre.wim /a
@echo.
@echo == Register the location of the recovery tools ==
W:\Windows\System32\Reagentc /Setreimage /Path R:\Recovery\WindowsRE /Target W:\Windows
@echo == Note: Windows RE may appear as Disabled, this is OK.
REM
if errorlevel 1 (
                 set errorlevel=0
                 Set RECOVERYSTATUS=FAILED
                )
If %DeployDrivers%==YES Call %~dp0deploy-drivers.cmd