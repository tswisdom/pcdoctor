@echo off
setlocal enabledelayedexpansion
:Input
if not defined ID (
	cls
	x:\Windows\System32\JPEGView.exe x:\Windows\System32\Images\Input1.tif
	Call x:\Windows\System32\bin\Scripts\Function.bat AssetID
	for /f "tokens=1,2 delims=:" %%a in (x:\Windows\System32\bin\Logs\LocalVariables.txt) do if %%a==ID set ID=%%b
)

if not defined TechnicianUserName (
	cls
	set /p TechnicianUserName=Scan Technician ID: 
	echo TechnicianUserName:!TechnicianUserName!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
)

REM if not defined Location (
	REM cls
	REM set /p Location= Scan Asset Location: 
	REM ECHO !Location!>len.txt
	REM FOR %%? IN (len.txt) DO ( SET /A strlength=%%~z? - 2 )
	REM if not !strlength!==6 (
		REM echo Invalid Asset Location, please re-scan
		REM set /p Location= Scan Asset Location: 
	REM )
	REM echo Location:!Location!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
REM )
set Location=010101
echo Location:!Location!>>x:\Windows\System32\bin\Logs\LocalVariables.txt

if not defined Cgrade (
	cls
	echo Please specify the units cosmetic grade:
	echo 1. A
	echo 2. B
	echo 3. C
	echo 4. D
	choice /C 1234 /N

	if !ERRORLEVEL!==4 (
		set Cgrade=D&&echo Cgrade:!Cgrade!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		if not %skipcosfail%==yes (
			set techcut=False
			echo techcut:!techcut!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		)
	)
	
	if !ERRORLEVEL!==3 (
		set Cgrade=C&&echo Cgrade:!Cgrade!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		if not %skipcosfail%==yes (
			set techcut=False
			echo techcut:!techcut!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		)
	)
	
		if !ERRORLEVEL!==2 (
		set Cgrade=B&&echo Cgrade:!Cgrade!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
	)
	
	if !ERRORLEVEL!==1 (
		set Cgrade=A&&echo Cgrade:!Cgrade!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
	)
)



if not !Headless!==True (
	if not defined Adapter (
		cls
		choice /m "Does this unit include an AC Adapter?"
		if ERRORLEVEL 2 (
		set Adapter=Not-Installed&&echo Adapter:!Adapter!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		set Incomplete=True&&echo Incomplete:!Incomplete!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		) else (
			set Adapter=Installed&&echo Adapter:!Adapter!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		)
	)
	if not defined Ssize (
		Call x:\Windows\System32\bin\Scripts\Function.bat ScreenSize
	)
	if not defined KeyboardLanguage (
		cls
		echo 1. EN-US [English United States]
		echo 2. EN-GB [English United Kingdom]
		echo 3. ES-ES [Spanish Traditional]
		echo 4. Not-Installed
		echo.
		choice /c 1234 /n /m "Please select the keyboard language."
		
		if !ERRORLEVEL!==1 set KeyboardLanguage=EN-US
		if !ERRORLEVEL!==2 set KeyboardLanguage=EN-GB
		if !ERRORLEVEL!==3 set KeyboardLanguage=ES-ES
		if !ERRORLEVEL!==4 (
			set Incomplete=True&&echo Incomplete:!Incomplete!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
			set KeyboardLanguage=Not-Installed
		)
		echo KeyboardLanguage:!KeyboardLanguage!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
	)	
)

if not defined techcut (
	cls
	echo This machine contains the following CPU: !cpu!
	choice /m "This machine has been cosmetically Graded as !Cgrade!, does this CPU fall within tech cuts?"
	if !errorlevel!==2 set techcut=False
	if !errorlevel!==1 set techcut=True
	echo techcut:!techcut!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
)

if %Unattend%==Undetermined (
	if %techcut%==True (
		cls
		choice /m "Would you like this unit to run Wipe, Diagnostics and Imaging unattended?"
		if ERRORLEVEL 2 (
			set Unattend=False
		) else (
			set Unattend=True
		)
		echo Unattend:!Unattend!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
	)
)
echo TechnicianUserName:!TechnicianUserName!>>x:\Windows\System32\bin\Logs\input.txt
echo Location:!Location!>>x:\Windows\System32\bin\Logs\input.txt
echo Cgrade:!Cgrade!>>x:\Windows\System32\bin\Logs\input.txt
echo techcut:!techcut!>>x:\Windows\System32\bin\Logs\input.txt
echo KeyboardLanguage:!KeyboardLanguage!>>x:\Windows\System32\bin\Logs\input.txt
echo Ssize:!Ssize!>>x:\Windows\System32\bin\Logs\input.txt
echo Adapter:!Adapter!>>x:\Windows\System32\bin\Logs\input.txt
echo Incomplete:!Incomplete!>>x:\Windows\System32\bin\Logs\input.txt
echo Unattend:!Unattend!>>x:\Windows\System32\bin\Logs\input.txt

exit