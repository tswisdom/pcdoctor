:ScreenSize
cls
set tempvar=0
set /p Ssize=Please specify the unit's Screen Size: 

if !Ssize! GEQ 10 (
	if !Ssize! LEQ 23 (
		set tempvar=1
		set Ssize=!Ssize!
		echo Ssize:!Ssize!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
	)
)

if NOT !tempvar!==1 (
	echo You have specified an invalid screen size!
	timeout 3 > nul
	goto ScreenSize
)

:EOF