:Wipe
azcopy /Y /@:"x:\Windows\System32\bin\scripts\azcopyanswer.txt" /pattern:"%ID%-WIPE.csv" > nul
if !errorlevel! NEQ 0 (
	color c0
	echo PC Doctor failed to upload Wipe Results.
	echo Press any key to retry upload. && pause > nul
	goto Wipe
	color
	cls
)
:EOF