@echo off
setlocal enabledelayedexpansion
goto %1

:AssetID
set /p ID=Scan Asset ID: 
set ID=!ID: =!
ECHO !ID!>len.txt
FOR %%? IN (len.txt) DO ( SET /A strlength=%%~z? - 2 )
if not !strlength!==10 (
	echo Invalid Asset ID, please re-scan
	goto AssetID
)
if /i %ID:~0,2% NEQ US (
	if /i %ID:~0,2% NEQ UK (
		echo Invalid Asset ID, please re-scan
		goto AssetID
	)
)
echo ID:!ID!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
echo ID:!ID!>>x:\Windows\System32\bin\Logs\input.txt
GOTO EOF

:ScreenSize
cls
set tempvar=0
set /p Ssize=Please specify the unit's Screen Size: 

if !Ssize! GEQ 10 (
	if !Ssize! LEQ 23 (
		set tempvar=1
		set Ssize=!Ssize!
		echo Ssize:!Ssize!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
	)
)

if NOT !tempvar!==1 (
	echo You have specified an invalid screen size!
	timeout 3 > nul
	goto ScreenSize
)
GOTO EOF

:InputLoop
if exist x:\Windows\System32\bin\Logs\input.txt (
	for /f "tokens=1,2 delims=:" %%a in (x:\Windows\System32\bin\Logs\input.txt) do set %%a=%%b
	set Input=True&&echo Input:!Input!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
	cls
) else (
	color c0
	echo Please finish providing inputs in the second terminal window
	echo and press any key to resume workflow. [Alt+Tab to change windows]
	pause>nul
	color
	cls
	goto InputLoop
)
GOTO EOF

:PCDSysInfo
pcd sysinfo -s %IP% -f>x:\Windows\System32\bin\Logs\!ID!-SYSINFO.yaml
if !errorlevel! NEQ 0 (
	color c0
	echo PC Doctor failed to generate System Information.
	echo Press any key to retry.&&pause>nul
	goto PCDSysInfo
	color
	cls
)
for /f "tokens=2 delims=:" %%f in ('find /I /C "License Authentication Error" x:\Windows\System32\bin\Logs\!ID!-SYSINFO.yaml') do if %%f==1 (
	color c0
	echo PC Doctor failed to generate System Information.
	echo Press any key to retry.&&pause>nul
	goto PCDSysInfo
	color
	cls
)
GOTO EOF

:SysInfo
azcopy /Y /@:"x:\Windows\System32\bin\scripts\azcopyanswer.txt" /pattern:"!ID!-SYSINFO.yaml" > nul
if !errorlevel! NEQ 0 (
	color c0
	echo PC Doctor failed to upload System Information.
	echo Press any key to retry upload. && pause > nul
	goto SysInfo
	color
	cls
) else (
	set sysinfo=Pass&&echo sysinfo:!sysinfo!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
)
GOTO EOF

:Smart
azcopy /Y /@:"x:\Windows\System32\bin\scripts\azcopyanswer.txt" /pattern:"!ID!-SMART.csv" > nul
if !errorlevel! NEQ 0 (
	color c0
	echo PC Doctor failed to upload SMART Results.
	echo Press any key to retry upload. && pause > nul
	goto Smart
	color
	cls
)
set SmartUpload=Successful&&echo SmartUpload:!SmartUpload!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
goto EOF

:Wipe
azcopy /Y /@:"x:\Windows\System32\bin\scripts\azcopyanswer.txt" /pattern:"%ID%-WIPE.csv" > nul
if !errorlevel! NEQ 0 (
	color c0
	echo PC Doctor failed to upload Wipe Results.
	echo Press any key to retry upload. && pause > nul
	goto Wipe
	color
	cls
)
set WipeUpload=Successful&&echo WipeUpload:!WipeUpload!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
goto EOF

:GRVariables
azcopy /Y /@:"x:\Windows\System32\bin\scripts\azcopyanswer.txt"  /pattern:"%ID%-GRVariables.yaml">nul
if !errorlevel! NEQ 0 (
	color c0
	echo PC Doctor failed to upload GRID values.
	echo Press any key to retry upload.&&pause>nul
	goto GRVariables
	color
	cls
)
set GRUpload=Successful&&echo GRUpload:!GRUpload!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
goto EOF

:Diag
azcopy /Y /@:"x:\Windows\System32\bin\scripts\azcopyanswer.txt" /pattern:"%ID%-DIAG.csv" > nul
if !errorlevel! NEQ 0 (
	color c0
	echo PC Doctor failed to upload Diagnostic Results.
	choice /m "Would you like to retry upload?"
	if !errorlevel!==1 goto Diag
	color
	cls
)
set DiagUpload=Successful&&echo DiagUpload:!DiagUpload!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
goto EOF

:EOF