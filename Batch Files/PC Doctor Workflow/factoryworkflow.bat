@echo off
cd x:\Windows\System32\bin
SETLOCAL ENABLEDELAYEDEXPANSION
REM <Script Variables>

if exist x:\Windows\System32\bin\Logs\LocalVariables.txt (
	for /f "tokens=1,2 delims=:" %%a in (x:\Windows\System32\bin\Logs\LocalVariables.txt) do set %%a=%%b
	set SkipBootWipe=True
)

if not defined SmartUpload set SmartUpload=Undetermined
if not defined WipeUpload set WipeUpload=Undetermined
if not defined DiagUpload set DiagUpload=Undetermined
if not defined GRUpload set GRUpload=Undetermined
if not defined Input set Input=Undetermined
if not defined Headless set Headless=Undetermined
if not defined Incomplete set Incomplete=Undetermined
if not defined Unattend set Unattend=Undetermined
if not defined skipcosfail set skipcosfail=Undetermined
if not defined strlength set strlength=0
REM if not defined Location set /p Location= Scan Asset Location: 
if not defined smarttest set smarttest=Undetermined
if not defined Fgrade set Fgrade=Undetermined
if not defined Istatus set Istatus=Registered
if not defined diag set diag=Undetermined
if not defined erasure set erasure=Undetermined
if not defined sysinfo set sysinfo=Undetermined
if not defined testfail set testfail=Undetermined
if not defined testundetermined set testundetermined=Undetermined
if not defined testpassed set testpassed=Undetermined
REM </Script Variables>

if not defined IP (
	cls
	echo Determining server IP Address, please wait...
	ipconfig | find "Default Gateway">ip.txt
	for /f "tokens=2 delims=:" %%. in (ip.txt) do set IP=%%.
	for /f "tokens=* delims= " %%a in ("%IP%") do set IP=%%a
	for /l %%a in (1,1,100) do if "!IP:~-1!"==" " set IP=!IP:~0,-1!
	echo IP:!IP!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
)

if not defined share (
	cls
	echo Determining server network share, please wait...
	if %IP%==172.26.0.1 set share=pc_doctor
	if %IP%==172.17.0.1 set share=Erasure
	if %IP%==172.18.0.1 set share=Erasure2
	if %IP%==172.19.0.1 set share=Enterprise
	echo share:!share!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
)

if not defined Chassis (
	cls
	echo Determining Chassis type, please wait...
	powershell -command "set-executionpolicy remotesigned"
	for /f "tokens=*" %%a in ('powershell -command "x:\Windows\System32\bin\Scripts\chassis.ps1"') do set Chassis=%%a
	if "!Chassis!"=="Desktop" set Headless=True
	if "!Chassis!"=="Low Profile Desktop" set Headless=True
	if "!Chassis!"=="Pizza Box" set Headless=True
	if "!Chassis!"=="Mini Tower" set Headless=True
	if "!Chassis!"=="Tower" set Headless=True
	if "!Chassis!"=="Space-Saving" set Headless=True
	if "!Chassis!"=="Lunch Box" set Headless=True
	if "!Chassis!"=="Main System Chassis" set Headless=True
	if "!Chassis!"=="Expansion Chassis" set Headless=True
	if "!Chassis!"=="Sub Chassis" set Headless=True
	if "!Chassis!"=="Bus Expansion Chassis" set Headless=True
	if "!Chassis!"=="Peripheral Chassis" set Headless=True
	if "!Chassis!"=="Storage Chassis" set Headless=True
	if "!Chassis!"=="Rack Mount Chassis" set Headless=True
	if "!Chassis!"=="Sealed-Case PC" set Headless=True
	echo Chassis:!Chassis!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
	echo Headless:!Headless!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
)

if not defined cpu (
	for /f "skip=1 delims=" %%. in ('wmic cpu get name') do for /f "delims=" %%A in ("%%.") do set cpu=%%A
	echo cpu:!cpu!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
)

if not defined CPUTier (
	set CPUTier=3
	for /f "tokens=1,2 delims=:" %%a in (x:\Windows\System32\bin\Scripts\CPUTier.csv) do if not /I "%cpu:%%a=%" EQU "%cpu%" set CPUTier=%%b
	echo CPUTier:!CPUTier!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
)

if not defined HDDMissing (
	REM HDD Check
	cls
	echo Checking system for Hard Drive, please wait...
	diskpart /s "x:\Windows\System32\bin\Scripts\DiskpartAnswer.txt" | find /c "Disk 0">HDDCount.txt
	for /f "tokens=*" %%f in (HDDCount.txt) do if %%f LSS 1 (
		set HDDMissing=True&&echo HDDMissing:!HDDMissing!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		set SkipBootWipe=True&&echo SkipBootWipe:!SkipBootWipe!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		set Incomplete=True&&echo Incomplete:!Incomplete!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
	) else (
		set HDDMissing=False&&echo HDDMissing:!HDDMissing!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
	)
)

if not !Headless!==True (
	if not defined Webcam (
		REM Webcam Check
		cls
		echo Checking system for Webcam, please wait...
		powershell -Command "(Get-CimInstance Win32_PnPEntity)">>Win32_PnPEntity.txt
		findstr /i "Camera Webcam Truevision iSight" Win32_PnPEntity.txt>>Webcam.txt
		for /f "tokens=3" %%f in ('find /v /c "" Webcam.txt') do if %%f GTR 0 (
			set Webcam=Installed&&echo Webcam:!Webcam!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		) else (
			set Webcam=Not-Installed&&echo Webcam:!Webcam!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		)
		del Win32_PnPEntity.txt
		del Webcam.txt
	)
	if not defined Battery (
		cls
		echo Checking system for Battery, please wait...
		powershell -Command "(Get-CimInstance Win32_Battery).caption">>Win32_Battery.txt
		for /f "tokens=3" %%f in ('find /v /c "" Win32_Battery.txt') do if %%f GTR 0 (
			set Battery=Installed&&echo Battery:!Battery!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		) else (
			set Battery=Not-Installed&&echo Battery:!Battery!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
			set Incomplete=True&&echo Incomplete:!Incomplete!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		)
		del Win32_Battery.txt
)

cls
echo Attempting to eject optical tray, please wait...
x:\Windows\System32\bin\Scripts\EjectDisk.vbs

REM <Brand Check>
if not defined brand (
	for /f "skip=1 delims=" %%. in ('wmic bios get manufacturer') do for /f "delims=" %%A in ("%%.") do set brand=%%A
	for /f "tokens=* delims= " %%a in ("%brand%") do set brand=%%a
	for /l %%a in (1,1,100) do if "!brand:~-1!"==" " set brand=!brand:~0,-1!
	echo brand:!brand!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
)

if not defined BrandTier (
	set Tier=3
	for /f "tokens=1,2 delims=:" %%a in (x:\Windows\System32\bin\Scripts\BrandTier.csv) do if /I "%brand%" EQU "%%a" set BrandTier=%%b
	echo BrandTier:!BrandTier!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
)

if %skipcosfail%==Undetermined (
	
	if /I "%brand%" EQU "Apple Inc." (
		set skipcosfail=yes&&echo skipcosfail:!skipcosfail!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		set techcut=True&&echo techcut:!techcut!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
	)
	
	if /I "%brand%" EQU "Alienware" (
		set skipcosfail=yes&&echo skipcosfail:!skipcosfail!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		set techcut=True&&echo techcut:!techcut!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
	)
	
)
REM </Brand Check>

if not defined SkipBootWipe (
	cls
	choice /c YN /t 30 /d N /m "Would you like to bypass boot wipe and proceed to the main menu?"
	set SkipBootWipe=!errorlevel!
	if !SkipBootWipe!==2 (
		set SkipBootWipe=False
	) else (
		set SkipBootWipe=True
	)
	echo SkipBootWipe:!SkipBootWipe!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
)

start "Input" /D x:\Windows\System32\bin x:\Windows\System32\bin\Scripts\Input.bat

:BootWipe
cls
if %HDDMissing%==True (
	if not !Input!==True Call x:\Windows\System32\bin\Scripts\Function.bat InputLoop
	if %sysinfo%==Undetermined (
		cls
		echo Generating system information log, please wait...
		pcd sysinfo -s %IP% -f>x:\Windows\System32\bin\Logs\!ID!-SYSINFO.yaml
		if !errorlevel! NEQ 0 (
			x:\Windows\System32\JPEGView.exe x:\Windows\System32\Images\Error2.tif
			color c0
			echo PC Doctor failed to generate System Information.
			echo Press any key to retry.&&pause > nul
			Call x:\Windows\System32\bin\Scripts\Function.bat PCDSysInfo
			color
			cls
		)
		for /f "tokens=2 delims=:" %%f in ('find /I /C "License Authentication Error" x:\Windows\System32\bin\Logs\!ID!-SYSINFO.yaml') do if %%f==1 Call x:\Windows\System32\bin\Scripts\Function.bat PCDSysInfo
		cls
		echo TechnicianUserName: !TechnicianUserName!>>x:\Windows\System32\bin\Logs\!ID!-SYSINFO.yaml
		echo AssetLocation: !Location!>>x:\Windows\System32\bin\Logs\!ID!-SYSINFO.yaml
		if exist x:\Windows\System32\bin\Logs\notes.txt (
			for /F "delims=" %%i in (x:\Windows\System32\bin\Logs\notes.txt) do set notes=!notes! %%i
			echo Notes: !notes!>>x:\Windows\System32\bin\Logs\!ID!-SYSINFO.yaml
		)
		echo Uploading System Information to the Cloud, please wait...
		azcopy /Y /@:"x:\Windows\System32\bin\scripts\azcopyanswer.txt" /pattern:"!ID!-SYSINFO.yaml" > nul 
		if !errorlevel! NEQ 0 (
			x:\Windows\System32\JPEGView.exe x:\Windows\System32\Images\Error3.tif
			color c0
			echo PC Doctor failed to upload System Information.
			echo Press any key to retry upload.&&pause > nul
			Call x:\Windows\System32\bin\Scripts\Function.bat SysInfo
			color
			cls
		) else (
			cls
			set sysinfo=Pass&&echo sysinfo:!sysinfo!>>x:\Windows\System32\bin\Logs\LocalVariables.txt&&echo sysinfo:!sysinfo!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		)
		pcd stop
	)
	color c0
	echo This unit is not reporting an installed Hard Drive, route to repair.
	echo.
	echo This machine contains the following CPU: !cpu!
	
	set Fgrade=Non-Functional&&echo Fgrade:!Fgrade!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
	set Istatus=Repair&&echo Istatus:!Istatus!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
	call x:\Windows\System32\bin\Scripts\GRVariableGen.bat
	x:\Windows\System32\JPEGView.exe x:\Windows\System32\Images\Route1.tif
	pause
	color
	goto MenuLoop
)
if %SkipBootWipe%==True goto MenuLoop
choice /c 13 /t 10 /d 1 /m "Please select single pass or triple pass wipe"
set passes=!errorlevel!&&echo passes:!passes!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
Set phase=Erasure&&echo phase:!phase!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
set Istatus=Wipe&&echo Istatus:!Istatus!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
pcdgui run -f "x:\Windows\System32\bin\scripts\custom\HDDHealth.xml" -efp -s %IP% -v -i TEMPID-SMART.csv
if !errorlevel!==23 (
	color c0
	choice /m "This unit failed one or more Hard Drive checks, would you like to proceed with the wipe?"
	if !errorlevel!==2 (
		if not !Input!==True Call x:\Windows\System32\bin\Scripts\Function.bat InputLoop
		pcd stop
		if %sysinfo%==Undetermined (
			cls
			echo Generating system information log, please wait...
			pcd sysinfo -s %IP% -f>x:\Windows\System32\bin\Logs\!ID!-SYSINFO.yaml
			if !errorlevel! NEQ 0 (
				x:\Windows\System32\JPEGView.exe x:\Windows\System32\Images\Error2.tif
				color c0
				echo PC Doctor failed to generate System Information.
				echo Press any key to retry.&&pause > nul
				Call x:\Windows\System32\bin\Scripts\Function.bat PCDSysInfo
				color
				cls
			)
			for /f "tokens=2 delims=:" %%f in ('find /I /C "License Authentication Error" x:\Windows\System32\bin\Logs\!ID!-SYSINFO.yaml') do if %%f==1 Call x:\Windows\System32\bin\Scripts\Function.bat PCDSysInfo
			cls
			echo TechnicianUserName: !TechnicianUserName!>>x:\Windows\System32\bin\Logs\!ID!-SYSINFO.yaml
			echo AssetLocation: !Location!>>x:\Windows\System32\bin\Logs\!ID!-SYSINFO.yaml
			if exist x:\Windows\System32\bin\Logs\notes.txt (
				for /F "delims=" %%i in (x:\Windows\System32\bin\Logs\notes.txt) do set notes=!notes! %%i
				echo Notes: !notes!>>x:\Windows\System32\bin\Logs\!ID!-SYSINFO.yaml
			)			
			echo Uploading System Information to the Cloud, please wait...
			azcopy /Y /@:"x:\Windows\System32\bin\scripts\azcopyanswer.txt" /pattern:"!ID!-SYSINFO.yaml" > nul 
			if !errorlevel! NEQ 0 (
				x:\Windows\System32\JPEGView.exe x:\Windows\System32\Images\Error3.tif
				color c0
				echo PC Doctor failed to upload System Information.
				echo Press any key to retry upload.&&pause > nul
				Call x:\Windows\System32\bin\Scripts\Function.bat SysInfo
				color
				cls
			) else (
				cls
				set sysinfo=Pass&&echo sysinfo:!sysinfo!>>x:\Windows\System32\bin\Logs\LocalVariables.txt&&echo sysinfo:!sysinfo!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
			)
			pcd stop
		)
		set smarttest=Fail&&echo smarttest:!smarttest!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		set Istatus=Repair&&echo Istatus:!Istatus!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		set Fgrade=Non-Functional&&echo Fgrade:!Fgrade!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		rename TEMPID-SMART.csv !ID!-SMART.csv
		move /Y "x:\Windows\System32\bin\!ID!-SMART.csv" "x:\Windows\System32\bin\Logs" 
		echo Uploading SMART results to the Cloud, please wait...
		azcopy /Y /@:"x:\Windows\System32\bin\scripts\azcopyanswer.txt" /pattern:"!ID!-SMART.csv" > nul 
		if !errorlevel! NEQ 0 (
			x:\Windows\System32\JPEGView.exe x:\Windows\System32\Images\Error3.tif
			color c0
			echo PC Doctor failed to upload SMART Results.
			echo Press any key to retry upload.&&pause > nul
			Call x:\Windows\System32\bin\Scripts\Function.bat Smart
			color
			cls
		)
		set SmartUpload=Successful&&echo SmartUpload:!SmartUpload!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		cls
		goto EL
	)
)
if !passes!==2 (
	pcdgui run -f "x:\Windows\System32\bin\scripts\custom\Erase3.xml" -efp -s %IP% -v -i TEMPID-WIPE.csv
) else (
	pcdgui run -f "x:\Windows\System32\bin\scripts\custom\Erase.xml" -efp -s %IP% -v -i TEMPID-WIPE.csv
)
cls
if not !Input!==True Call x:\Windows\System32\bin\Scripts\Function.bat InputLoop
if %sysinfo%==Undetermined (
	cls
	echo Generating system information log, please wait...
	pcd sysinfo -s %IP% -f>x:\Windows\System32\bin\Logs\!ID!-SYSINFO.yaml
	if !errorlevel! NEQ 0 (
		x:\Windows\System32\JPEGView.exe x:\Windows\System32\Images\Error2.tif
		color c0
		echo PC Doctor failed to generate System Information.
		echo Press any key to retry.&&pause > nul
		Call x:\Windows\System32\bin\Scripts\Function.bat PCDSysInfo
		color
		cls
	)
	for /f "tokens=2 delims=:" %%f in ('find /I /C "License Authentication Error" x:\Windows\System32\bin\Logs\!ID!-SYSINFO.yaml') do if %%f==1 Call x:\Windows\System32\bin\Scripts\Function.bat PCDSysInfo
	cls
	echo TechnicianUserName: !TechnicianUserName!>>x:\Windows\System32\bin\Logs\!ID!-SYSINFO.yaml
	echo AssetLocation: !Location!>>x:\Windows\System32\bin\Logs\!ID!-SYSINFO.yaml
	if exist x:\Windows\System32\bin\Logs\notes.txt (
		for /F "delims=" %%i in (x:\Windows\System32\bin\Logs\notes.txt) do set notes=!notes! %%i
		echo Notes: !notes!>>x:\Windows\System32\bin\Logs\!ID!-SYSINFO.yaml
	)	
	echo Uploading System Information to the Cloud, please wait...
	azcopy /Y /@:"x:\Windows\System32\bin\scripts\azcopyanswer.txt" /pattern:"!ID!-SYSINFO.yaml" > nul 
	if !errorlevel! NEQ 0 (
		x:\Windows\System32\JPEGView.exe x:\Windows\System32\Images\Error3.tif
		color c0
		echo PC Doctor failed to upload System Information.
		echo Press any key to retry upload.&&pause > nul
		Call x:\Windows\System32\bin\Scripts\Function.bat SysInfo
		color
		cls
	) else (
		cls
		set sysinfo=Pass&&echo sysinfo:!sysinfo!>>x:\Windows\System32\bin\Logs\LocalVariables.txt&&echo sysinfo:!sysinfo!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
	)
	pcd stop
)
rename TEMPID-SMART.csv %ID%-SMART.csv
rename TEMPID-WIPE.csv %ID%-WIPE.csv
move /Y "x:\Windows\System32\bin\%ID%-SMART.csv" "x:\Windows\System32\bin\Logs" > nul
move /Y "x:\Windows\System32\bin\%ID%-WIPE.csv" "x:\Windows\System32\bin\Logs" > nul
echo Uploading SMART results to the Cloud, please wait...
azcopy /Y /@:"x:\Windows\System32\bin\scripts\azcopyanswer.txt" /pattern:"!ID!-SMART.csv" > nul 
if !errorlevel! NEQ 0 (
	x:\Windows\System32\JPEGView.exe x:\Windows\System32\Images\Error3.tif
	color c0
	echo PC Doctor failed to upload SMART Results.
	echo Press any key to retry upload.&&pause > nul
	Call x:\Windows\System32\bin\Scripts\Function.bat Smart
	color
	cls
)
set SmartUpload=Successful&&echo SmartUpload:!SmartUpload!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
cls
echo TechnicianUserName: !TechnicianUserName!>>x:\Windows\System32\bin\Logs\!ID!-WIPE.csv
echo AssetLocation: !Location!>>x:\Windows\System32\bin\Logs\!ID!-WIPE.csv
if exist x:\Windows\System32\bin\Logs\notes.txt (
	for /F "delims=" %%i in (x:\Windows\System32\bin\Logs\notes.txt) do set notes=!notes! %%i
	echo Notes: !notes!>>x:\Windows\System32\bin\Logs\!ID!-WIPE.csv
)
echo Uploading Wipe results to the Cloud, please wait...
azcopy /Y /@:"x:\Windows\System32\bin\scripts\azcopyanswer.txt" /pattern:"%ID%-WIPE.csv"  > nul
if !errorlevel! NEQ 0 (
	x:\Windows\System32\JPEGView.exe x:\Windows\System32\Images\Error3.tif
	color c0
	echo PC Doctor failed to upload Wipe Results.
	echo Press any key to retry upload.&&pause > nul
	Call x:\Windows\System32\bin\Scripts\Function.bat Wipe
	color
	cls
)
set WipeUpload=Successful&&echo WipeUpload:!WipeUpload!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
cls
pcd stop
goto EL

:MenuLoop
cls
if not !Input!==True Call x:\Windows\System32\bin\Scripts\Function.bat InputLoop
cd x:\Windows\System32\bin
if !sysinfo!==Undetermined (
	cls
	echo Generating system information log, please wait...
	pcd sysinfo -s %IP% -f>x:\Windows\System32\bin\Logs\!ID!-SYSINFO.yaml
	if !errorlevel! NEQ 0 (
		x:\Windows\System32\JPEGView.exe x:\Windows\System32\Images\Error2.tif
		color c0
		echo PC Doctor failed to generate System Information.
		echo Press any key to retry.&&pause > nul
		Call x:\Windows\System32\bin\Scripts\Function.bat PCDSysInfo
		color
		cls
	)
	for /f "tokens=2 delims=:" %%f in ('find /I /C "License Authentication Error" x:\Windows\System32\bin\Logs\!ID!-SYSINFO.yaml') do if %%f==1 Call x:\Windows\System32\bin\Scripts\Function.bat PCDSysInfo
	cls
	echo TechnicianUserName: !TechnicianUserName!>>x:\Windows\System32\bin\Logs\!ID!-SYSINFO.yaml
	echo AssetLocation: !Location!>>x:\Windows\System32\bin\Logs\!ID!-SYSINFO.yaml
	if exist x:\Windows\System32\bin\Logs\notes.txt (
		for /F "delims=" %%i in (x:\Windows\System32\bin\Logs\notes.txt) do set notes=!notes! %%i
		echo Notes: !notes!>>x:\Windows\System32\bin\Logs\!ID!-SYSINFO.yaml
	)	
	echo Uploading System Information to the Cloud, please wait...
	azcopy /Y /@:"x:\Windows\System32\bin\scripts\azcopyanswer.txt" /pattern:"!ID!-SYSINFO.yaml" > nul 
	if !errorlevel! NEQ 0 (
		x:\Windows\System32\JPEGView.exe x:\Windows\System32\Images\Error3.tif
		color c0
		echo PC Doctor failed to upload System Information.
		echo Press any key to retry upload.&&pause > nul
		Call x:\Windows\System32\bin\Scripts\Function.bat SysInfo
		color
		cls
	) else (
		cls
		set sysinfo=Pass&&echo sysinfo:!sysinfo!>>x:\Windows\System32\bin\Logs\LocalVariables.txt&&echo sysinfo:!sysinfo!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
	)
	pcd stop
)
cls
echo Main Menu:
echo Select an option.
echo 1. Run PC Doctor Script.
echo 2. Apply Windows Image.
echo 3. Change Asset Configuration.
echo 4. Manage PC Doctor Logs.
echo 5. Exit.
echo 6. Shutdown.
echo.
echo Asset Status:
echo Asset ID: %ID%
echo Asset Status: !Istatus!
echo Functional Grade: !Fgrade!
echo Cosmetic Grade: !Cgrade!
if defined aux1 echo Auxiliary Field 1: !aux1!
if defined aux2 echo Auxiliary Field 2: !aux2!
if defined notes echo Notes: !notes!
echo.
echo Attribute Values:
echo CPU: !cpu!
if defined Webcam echo Webcam: %Webcam%
if defined Ssize echo Screen Size: !Ssize!
if defined KeyboardLanguage echo Keyboard Language: !KeyboardLanguage!
if defined Adapter echo AC Adapter: !Adapter!





if %HDDMissing%==True (
	echo This unit is not reporting an installed Hard Drive.
) else (
	if %erasure%==Undetermined echo This unit has not been wiped.
	if %erasure%==Fail echo This unit has failed to wipe.
	if %erasure%==Pass echo This unit has been successfully wiped.
)
if %diag%==Undetermined echo This unit has not been run through diagnostics.
if %diag%==Fail (
	echo This unit has failed one or more diagnostic tests.
	if !DiagUpload!==Successful echo The Diagnostic results were uploaded successfully.
	if !DiagUpload!==Undetermined echo The Diagnostic results have not been uploaded.
)
if %diag%==Pass (
	echo This unit has successfully passed all diagnostic tests.
	if !DiagUpload!==Successful echo The Diagnostic results were uploaded successfully.
	if !DiagUpload!==Undetermined echo The Diagnostic results have not been uploaded.
)
choice /c 123456 /n
set workflow=%errorlevel%


if %workflow%==1 (
	goto PhaseLoop
)

if %workflow%==2 (
	if /I "%brand%" EQU "Apple Inc." (
		echo This machine is branded as an Apple device.
		echo Please insert MacOS USB and reboot to apply image.
		call x:\Windows\System32\bin\scripts\GRVariableGen.bat
		pause
		goto MenuLoop
	)
	
	call z:\scripts\menu-64
	goto MenuLoop
)

if %workflow%==3 (
	goto GRIDLoop
)

if %workflow%==4 (
	goto Logs
)

if %workflow%==5 (
	goto EOF
)

if %workflow%==6 (
	wpeutil Shutdown
)
:PhaseLoop
cls
cd x:\Windows\System32\bin
echo Select PC Doctor Script:
echo 1. Erasure.
echo 2. Diagnostic.
echo 3. Launch PC Doctor without a script.
echo 4. Return to Main Menu.
choice /c 1234 /n
set phase=%errorlevel%

if %phase%==1 (
	choice /c 13 /t 10 /d 1 /m "Please select single pass or triple pass wipe"
	set passes=!errorlevel!&&echo passes:!passes!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
	Set phase=Erasure&&echo phase:!phase!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
	set Istatus=Wipe&&echo Istatus:!Istatus!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
	pcdgui run -f "x:\Windows\System32\bin\scripts\custom\HDDHealth.xml" -efp -s %IP% -v -i "x:\Windows\System32\bin\Logs\%ID%-SMART.csv"
	if !errorlevel!==23 (
		color c0
		choice /m "This unit failed one or more Hard Drive checks, would you like to proceed with the wipe?"
		if !errorlevel!==2 (
			pcd stop
			set smarttest=Fail&&echo smarttest:!smarttest!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
			set Istatus=Repair&&echo Istatus:!Istatus!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
			set Fgrade=Non-Functional&&echo Fgrade:!Fgrade!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
			echo Uploading SMART results to the Cloud, please wait...
			azcopy /Y /@:"x:\Windows\System32\bin\scripts\azcopyanswer.txt" /pattern:"!ID!-SMART.csv" > nul 
			if !errorlevel! NEQ 0 (
				x:\Windows\System32\JPEGView.exe x:\Windows\System32\Images\Error3.tif
				color c0
				echo PC Doctor failed to upload SMART Results.
				echo Press any key to retry upload.&&pause > nul
				Call x:\Windows\System32\bin\Scripts\Function.bat Smart
				color
				cls
			)
			set SmartUpload=Successful&&echo SmartUpload:!SmartUpload!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
			cls
			goto EL
		)
	)
	echo Uploading SMART results to the Cloud, please wait...
	azcopy /Y /@:"x:\Windows\System32\bin\scripts\azcopyanswer.txt" /pattern:"!ID!-SMART.csv" > nul 
	if !errorlevel! NEQ 0 (
		x:\Windows\System32\JPEGView.exe x:\Windows\System32\Images\Error3.tif
		color c0
		echo PC Doctor failed to upload SMART Results.
		echo Press any key to retry upload.&&pause > nul
		Call x:\Windows\System32\bin\Scripts\Function.bat Smart
		color
		cls
	)
	set SmartUpload=Successful&&echo SmartUpload:!SmartUpload!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
	cls
	if !passes!==2 (
		pcdgui run -f "x:\Windows\System32\bin\scripts\custom\Erase3.xml" -efp -s %IP% -v -i "x:\Windows\System32\bin\Logs\%ID%-WIPE.csv"
	) else (
		pcdgui run -f "x:\Windows\System32\bin\scripts\custom\Erase.xml" -efp -s %IP% -v -i "x:\Windows\System32\bin\Logs\%ID%-WIPE.csv"
	)
	echo TechnicianUserName: !TechnicianUserName!>>x:\Windows\System32\bin\Logs\!ID!-WIPE.csv
	echo AssetLocation: !Location!>>x:\Windows\System32\bin\Logs\!ID!-WIPE.csv
	if exist x:\Windows\System32\bin\Logs\notes.txt (
		for /F "delims=" %%i in (x:\Windows\System32\bin\Logs\notes.txt) do set notes=!notes! %%i
		echo Notes: !notes!>>x:\Windows\System32\bin\Logs\!ID!-WIPE.csv
	)
	echo Uploading Wipe results to the Cloud, please wait...
	azcopy /Y /@:"x:\Windows\System32\bin\scripts\azcopyanswer.txt" /pattern:"%ID%-WIPE.csv"  > nul
	if !errorlevel! NEQ 0 (
		x:\Windows\System32\JPEGView.exe x:\Windows\System32\Images\Error3.tif
		color c0
		echo PC Doctor failed to upload Wipe Results.
		echo Press any key to retry upload.&&pause > nul
		Call x:\Windows\System32\bin\Scripts\Function.bat Wipe
		color
		cls
	)
	set WipeUpload=Successful&&echo WipeUpload:!WipeUpload!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
	cls
	pcd stop
	goto EL
)

if %phase%==2 (
	Set phase=Diagnostic&&echo phase:!phase!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
	set Istatus=Test&&echo Istatus:!Istatus!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
	pcdgui run -f "x:\Windows\System32\bin\scripts\custom\Functional.xml" -efp -s %IP% -v -i "x:\Windows\System32\bin\Logs\%ID%-DIAG.csv"
	echo TechnicianUserName: !TechnicianUserName!>>x:\Windows\System32\bin\Logs\!ID!-DIAG.csv
	echo AssetLocation: !Location!>>x:\Windows\System32\bin\Logs\!ID!-DIAG.csv
	if exist x:\Windows\System32\bin\Logs\notes.txt (
		for /F "delims=" %%i in (x:\Windows\System32\bin\Logs\notes.txt) do set notes=!notes! %%i
		echo Notes: !notes!>>x:\Windows\System32\bin\Logs\!ID!-DIAG.csv
	)
	echo Uploading Diagnostic results to the Cloud, please wait...
	azcopy /Y /@:"x:\Windows\System32\bin\scripts\azcopyanswer.txt" /pattern:"%ID%-DIAG.csv" > nul
	if !errorlevel! NEQ 0 (
		x:\Windows\System32\JPEGView.exe x:\Windows\System32\Images\Error3.tif
		color c0
		echo PC Doctor failed to upload Diagnostic Results.
		echo Press any key to retry upload.&&pause > nul
		Call x:\Windows\System32\bin\Scripts\Function.bat Diag
		color
		cls
	)
	set DiagUpload=Successful&&echo DiagUpload:!DiagUpload!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
	cls
	pcd stop
	goto EL
)

if %phase%==3 (
	pcdgui run -s %IP%
	cls
	pcd stop
	echo Returning to main menu.
	timeout 3 > nul 
	goto MenuLoop
)

if %phase%==4 (
	goto MenuLoop
)

:EL
cls
cd x:\Windows\System32\bin\Logs
if %phase%==Erasure (
	if exist %ID%-WIPE.csv (
		for /f "tokens=3 delims= " %%b in ('find /i /c "Completed" %ID%-WIPE.csv') do set testpassed=%%b
		if !testpassed! gtr 0 (
			set erasure=Pass&&echo erasure:!erasure!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
			if not %Fgrade%==Functional-Incomplete set Fgrade=Functional&&echo Fgrade:!Fgrade!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
			goto CosmeticLoop
		) else (
		set erasure=Fail&&echo erasure:!erasure!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		echo HDD_Fail/>>x:\Windows\System32\bin\Logs\notes.txt
		echo Not_Wiped/>>x:\Windows\System32\bin\Logs\notes.txt
		set Fgrade=Non-Functional&&echo Fgrade:!Fgrade!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		goto TestFail
		)
	) else (
		set erasure=Fail&&echo erasure:!erasure!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		echo HDD_Fail/>>x:\Windows\System32\bin\Logs\notes.txt
		echo Not_Wiped/>>x:\Windows\System32\bin\Logs\notes.txt
		set Fgrade=Non-Functional&&echo Fgrade:!Fgrade!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		goto TestFail
	)
)

if %phase%==Diagnostic (
	REM Checks for any test results marked as failed
	for /f "tokens=3 delims= " %%a in ('find /i /c "Fail" %ID%-DIAG.csv') do set testfail=%%a
	REM Checks for any test results not marked as passed
	if exist error.csv del error.csv
	for /f "skip=2 tokens=3 delims=," %%a in (%ID%-DIAG.csv) do echo %%a>>error.csv
	for /f "tokens=3 delims= " %%b in ('find /i /v /c "Passed" error.csv') do set testundetermined=%%b
	
	if !testfail! gtr 0 (
		set Fgrade=Non-Functional&&echo Fgrade:!Fgrade!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		set diag=Fail&&echo diag:!diag!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		GOTO TestFail
	)

	if not !testundetermined! GTR 0 (
		set diag=Pass&&echo diag:!diag!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		if not %Fgrade%==Functional-Incomplete set Fgrade=Functional&&echo Fgrade:!Fgrade!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		GOTO CosmeticLoop
	) else (
		set status=Undetermined
		goto UndeterminedLoop
	)
)

:UndeterminedLoop
cls
color e0
echo The results of one or more tests were undetermined.
echo Please manually confirm test results:
echo 1. Pass
echo 2. Fail
choice /c 12 /n
set status=%errorlevel%

if %status%==1 (
	color
	if %phase%==Diagnostic set diag=Pass&&echo diag:!diag!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
	if not %Fgrade%==Functional-Incomplete set Fgrade=Functional&&echo Fgrade:!Fgrade!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
	goto CosmeticLoop
)

if %status%==2 (
	set Fgrade=Non-Functional&&echo Fgrade:!Fgrade!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
	if %phase%==Diagnostic set diag=Fail&&echo diag:!diag!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
	goto TestFail
)

:TestFail
REM Machine is pushed to TestFail loop if it fails at any point.
cls
cd x:\Windows\System32\bin\Logs
color c0
if exist !ID!-DIAG.csv (
	for /f "skip=2 delims=" %%a in ('find /i "Fail" !ID!-DIAG.csv') do echo %%a>failed.csv
	for /f "skip=2 delims=" %%a in ('find "Battery:" failed.csv') do echo %%a>battery.csv
	for /f "skip=2 delims=" %%a in ('find "Network Card:" failed.csv') do echo %%a>network.csv
	for /f "skip=2 delims=" %%a in ('find "CPU:" failed.csv') do echo %%a>CPU.csv
	for /f "skip=2 delims=" %%a in ('find "Memory:" failed.csv') do echo %%a>memory.csv
	for /f "skip=2 delims=" %%a in ('find "Hard Drive:" failed.csv') do echo %%a>HDD.csv
	echo The following components have failed:
	if exist battery.csv (
		echo Battery
	)
	if exist network.csv (
		echo NIC
		echo NIC_Fail/>>x:\Windows\System32\bin\Logs\notes.txt
	)
	if exist CPU.csv (
		echo CPU
		echo MBB_Fail/>>x:\Windows\System32\bin\Logs\notes.txt
	)
	if exist memory.csv echo RAM
	if exist HDD.csv (
		echo Hard Drive
		echo HDD_Fail/>>x:\Windows\System32\bin\Logs\notes.txt
	)
)
if defined smarttest if !smarttest!==Fail echo This unit failed the HDD health test and has not been wiped.
if defined erasure if !erasure!==Fail echo This unit failed to wipe successfully.
echo.


echo This machine contains the following CPU: !cpu!


echo This machine failed one or more functional tests, route to repair.
set Fgrade=Non-Functional&&echo Fgrade:!Fgrade!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
set Istatus=Repair&&echo Istatus:!Istatus!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
call x:\Windows\System32\bin\Scripts\GRVariableGen.bat
x:\Windows\System32\JPEGView.exe x:\Windows\System32\Images\Route1.tif
pause 
color
cd x:\Windows\System32\bin
goto MenuLoop

:CosmeticLoop
REM cls
REM if %Cgrade%==U (
	REM echo Please specify the units cosmetic grade:
	REM echo 1. A
	REM echo 2. B
	REM echo 3. C
	REM echo 4. D
	REM choice /C 1234 /N&set Cgrade=!errorlevel!

	REM if !Cgrade!==1 (
		REM set Cgrade=A&&echo Cgrade:!Cgrade!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		REM goto CPULoop
	REM )

	REM if !Cgrade!==2 (
		REM set Cgrade=B&&echo Cgrade:!Cgrade!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		REM goto CPULoop
	REM )

	REM if !Cgrade!==3 (
		REM set Cgrade=C&&echo Cgrade:!Cgrade!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		REM if %skipcosfail%==yes goto CPULoop
		REM set techcut=False&&echo techcut:!techcut!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		REM x:\Windows\System32\JPEGView.exe x:\Windows\System32\Images\Route1.tif
		REM color c0
		REM echo.
		REM echo This machine does not meet cosmetic requirements for imaging, process as bulk.
		REM pause
		REM color
		REM goto MenuLoop
	REM )

	REM if !Cgrade!==4 (
		REM set Cgrade=D&&echo Cgrade:!Cgrade!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		REM if %skipcosfail%==yes goto CPULoop
		REM set techcut=False&&echo techcut:!techcut!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		REM x:\Windows\System32\JPEGView.exe x:\Windows\System32\Images\Route1.tif
		REM color c0
		REM echo.
		REM echo This machine does not meet cosmetic requirements for imaging, process as bulk.
		REM pause
		REM color
		REM goto MenuLoop
	REM )

REM )

:CPULoop
cls
if %techcut%==Undetermined (
	
	
	echo This machine contains the following CPU: !cpu!
	choice /m "This machine has been cosmetically Graded as !Cgrade!, does this CPU fall within tech cuts [Y,N]?"
	if !errorlevel!==2 set techcut=False
	if !errorlevel!==1 set techcut=True
	echo techcut:!techcut!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
)
if %techcut%==True (
	if %diag%==Pass (
		if %Unattend%==True (
			REM <Auto-Image>
			if /I "%brand%" EQU "Apple Inc." (
				echo This machine is branded as an Apple device.
				echo Please insert MacOS USB and reboot to apply image.
				call x:\Windows\System32\bin\scripts\GRVariableGen.bat
				pause
				goto MenuLoop
			)
			echo Initializing MAR Imaging Script.
			timeout 3 > nul 
			call z:\scripts\menu-64
			goto MenuLoop
			REM </Auto-Image>
			)
		choice /m "This machine meets all requirements for imaging. Would you like to apply an image now?"

		if ERRORLEVEL 2 (
			echo.
			echo Returning to main menu.
			timeout 3 > nul 
			goto MenuLoop
		)
		
		if ERRORLEVEL 1 (
			if /I "%brand%" EQU "Apple Inc." (
				echo This machine is branded as an Apple device.
				echo Please insert MacOS USB and reboot to apply image.
				call x:\Windows\System32\bin\scripts\GRVariableGen.bat
				pause
				goto MenuLoop
			)
			call z:\scripts\menu-64
		)
	) else (
		if %Unattend%==True (
			REM <Auto-Diag>
			Set phase=Diagnostic&&echo phase:!phase!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
			set Istatus=Test&&echo Istatus:!Istatus!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
			cd x:\Windows\System32\bin
			echo Initializing PC Doctor Diagnostic Script.
			timeout 3 > nul 
			pcdgui run -f "x:\Windows\System32\bin\scripts\custom\Functional.xml" -efp -s %IP% -v -i "x:\Windows\System32\bin\Logs\%ID%-DIAG.csv"
			echo TechnicianUserName: !TechnicianUserName!>>x:\Windows\System32\bin\Logs\!ID!-DIAG.csv
			echo AssetLocation: !Location!>>x:\Windows\System32\bin\Logs\!ID!-DIAG.csv
			if exist x:\Windows\System32\bin\Logs\notes.txt (
				for /F "delims=" %%i in (x:\Windows\System32\bin\Logs\notes.txt) do set notes=!notes! %%i
				echo Notes: !notes!>>x:\Windows\System32\bin\Logs\!ID!-DIAG.csv
			)
			echo Uploading Diagnostic results to the Cloud, please wait...
			azcopy /Y /@:"x:\Windows\System32\bin\scripts\azcopyanswer.txt" /pattern:"%ID%-DIAG.csv" > nul 
			if !errorlevel! NEQ 0 (
				x:\Windows\System32\JPEGView.exe x:\Windows\System32\Images\Error3.tif
				color c0
				echo PC Doctor failed to upload Diagnostic Results.
				echo Press any key to retry upload.&&pause > nul
				Call x:\Windows\System32\bin\Scripts\Function.bat Diag
				color
				cls
			)
			set DiagUpload=Successful&&echo DiagUpload:!DiagUpload!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
			cls
			pcd stop
			goto EL
			REM </Auto-Diag>
		)
		REM <Choice-Diag>
		choice /m "Would you like to run PC Doctor Diagnostic Test?"
		if not ERRORLEVEL 2 (
			Set phase=Diagnostic&&echo phase:!phase!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
			set Istatus=Test&&echo Istatus:!Istatus!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
			cd x:\Windows\System32\bin
			pcdgui run -f "x:\Windows\System32\bin\scripts\custom\Functional.xml" -efp -s %IP% -v -i "x:\Windows\System32\bin\Logs\%ID%-DIAG.csv"
			echo TechnicianUserName: !TechnicianUserName!>>x:\Windows\System32\bin\Logs\!ID!-DIAG.csv
			echo AssetLocation: !Location!>>x:\Windows\System32\bin\Logs\!ID!-DIAG.csv
			if exist x:\Windows\System32\bin\Logs\notes.txt (
				for /F "delims=" %%i in (x:\Windows\System32\bin\Logs\notes.txt) do set notes=!notes! %%i
				echo Notes: !notes!>>x:\Windows\System32\bin\Logs\!ID!-DIAG.csv
			)
			echo Uploading Diagnostic results to the Cloud, please wait...
			azcopy /Y /@:"x:\Windows\System32\bin\scripts\azcopyanswer.txt" /pattern:"%ID%-DIAG.csv" > nul 
			if !errorlevel! NEQ 0 (
				x:\Windows\System32\JPEGView.exe x:\Windows\System32\Images\Error3.tif
				color c0
				echo PC Doctor failed to upload Diagnostic Results.
				echo Press any key to retry upload.&&pause > nul
				Call x:\Windows\System32\bin\Scripts\Function.bat Diag
				color
				cls
			)
			set DiagUpload=Successful&&echo DiagUpload:!DiagUpload!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
			cls
			pcd stop
			goto EL
		)
		color c0
		echo.
		set Istatus=BATCHING&&echo Istatus:!Istatus!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		echo This machine contains the following CPU: !cpu!
		echo This machine does not meet requirements for imaging, process as bulk.
		call x:\Windows\System32\bin\scripts\GRVariableGen.bat
		pause
		color
		goto MenuLoop
		REM </Choice-Diag>
	)
)

if %techcut%==False (
	color c0
	echo.
	set Istatus=BATCHING&&echo Istatus:!Istatus!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
	echo This machine contains the following CPU: !cpu!
	echo This machine does not meet requirements for imaging, process as bulk.
	call x:\Windows\System32\bin\scripts\GRVariableGen.bat
	x:\Windows\System32\JPEGView.exe x:\Windows\System32\Images\Route1.tif
	pause
	color
	goto MenuLoop
)

:Logs
cls
cd "x:\Windows\System32\bin\Logs"
echo 1. View Logs.
echo 2. Re-upload Logs.
echo 3. Return To Main Menu.
echo.
choice /c 123 /n

if !ERRORLEVEL!==3 goto MenuLoop
if !ERRORLEVEL!==2 goto UpLogs
if !ERRORLEVEL!==1 goto ViewLogs



:ViewLogs
cls
if exist %ID%-SYSINFO.yaml (
	echo 1. System Information Log
) else (
	echo X. System Information Log is unavailable.
)

if exist %ID%-SMART.csv (
	echo 2. Hard Drive Health Log
) else (
	echo X. Hard Drive Health Log is unavailable.
)	

if exist %ID%-WIPE.csv (
	echo 3. Hard Drive Wipe Log
) else (
	echo X. Hard Drive Wipe Log is unavailable.
)	

if exist %ID%-DIAG.csv (
	echo 4. System Diagnostics Log
) else (
	echo X. System Diagnostics Log is unavailable.
)

echo 5. GRID Variables Log


echo 6. Return to manage logs.

echo.
choice /c 123456 /n /m "Please select the log file you would like to view."

if ERRORLEVEL 6 (
	goto Logs
)

if ERRORLEVEL 5 (
	cls
	if defined TechnicianUserName echo Technician ID: %TechnicianUserName%
	if defined AssetLocation echo Asset Location: %AssetLocation%
	if defined Istatus echo Inventory Status: %Istatus%
	if defined aux1 echo Auxiliary Field 1: %aux1%
	if defined aux2 echo Auxiliary Field 2: %aux2%
	if defined notes echo Notes: %notes%
	if defined Cgrade echo Cosmetic Grade: %Cgrade%
	if defined Fgrade echo Functional Grade: %Fgrade%
	if defined Webcam echo Webcam: %Webcam%
	if defined KeyboardLanguage echo Keyboard Language: %KeyboardLanguage%
	if defined Ssize echo Screen Size: %Ssize%
	if defined Adapter echo AC Adapter: %Adapter%
	notepad %ID%-GRVariables.yaml
	echo.
	echo Press any key to return to View Logs.
	pause > nul
	goto ViewLogs
)

if ERRORLEVEL 4 (
	if exist %ID%-DIAG.csv (
		notepad %ID%-DIAG.csv
		goto ViewLogs
	) else (
		echo System Diagnostics Log is unavailable.
		timeout 3 > nul 
		goto ViewLogs
	)
)

if ERRORLEVEL 3 (
	if exist %ID%-WIPE.csv (
		notepad %ID%-WIPE.csv
		goto ViewLogs
	) else (
		echo Hard Drive Wipe Log is unavailable.
		timeout 3 > nul 
		goto ViewLogs
	)
)

if ERRORLEVEL 2 (
	if exist %ID%-SMART.csv (
		notepad %ID%-SMART.csv
		goto ViewLogs
	) else (
		echo Hard Drive Health Log is unavailable.
		timeout 3 > nul 
		goto ViewLogs
	)
)

if ERRORLEVEL 1 (
	if exist %ID%-SYSINFO.yaml (
		notepad %ID%-SYSINFO.yaml
		goto ViewLogs
	) else (
		echo System Information Log is unavailable.
		timeout 3 > nul 
		goto ViewLogs
	)
)

:UpLogs
cls
echo 1. System Information Log

if exist %ID%-SMART.csv (
	echo 2. Hard Drive Health Log
) else (
	echo X. Hard Drive Health Log is unavailable.
)	

if exist %ID%-WIPE.csv (
	echo 3. Hard Drive Wipe Log
) else (
	echo X. Hard Drive Wipe Log is unavailable.
)	

if exist %ID%-DIAG.csv (
	echo 4. System Diagnostics Log
) else (
	echo X. System Diagnostics Log is unavailable.
)

REM if exist %ID%-GRVariables.yaml (
echo 5. GRID Variables Log
REM ) 
REM else (
	REM echo 5. GRID Variables Log
	REM if %Fgrade%==Functional if %Incomplete%==True set Fgrade=Functional-Incomplete&&echo Fgrade:!Fgrade!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
	REM for /f "tokens=1,2 delims=:" %%a in (x:\Windows\System32\bin\scripts\IstatusCodes.csv) do if %Istatus%==%%a set IstatusCode=%%b
	REM for /f "tokens=1,2 delims=:" %%a in (x:\Windows\System32\bin\scripts\FgradeCodes.csv) do if %Fgrade%==%%a set FgradeCode=%%b
	REM for /f "tokens=1,2 delims=:" %%a in (x:\Windows\System32\bin\scripts\CgradeCodes.csv) do if %Cgrade%==%%a set CgradeCode=%%b
	REM if exist notes.txt for /F "delims=" %%i in (x:\Windows\System32\bin\Logs\notes.txt) do set notes=!notes! %%i
	REM if defined notes set comments=%notes%&&echo notes:!notes!>>x:\Windows\System32\bin\Logs\LocalVariables.txt

	REM echo TechnicianUserName: %TechnicianUserName%>x:\Windows\System32\bin\Logs\%ID%-GRVariables.yaml
	REM echo AssetLocation: %Location%>>x:\Windows\System32\bin\Logs\%ID%-GRVariables.yaml
	REM echo AssetStatus: %IstatusCode%>>x:\Windows\System32\bin\Logs\%ID%-GRVariables.yaml
	REM if defined Aux1 (
		REM echo AuxField1: %Aux1%>>x:\Windows\System32\bin\Logs\%ID%-GRVariables.yaml
	REM ) else (
		REM echo AuxField1:>>x:\Windows\System32\bin\Logs\%ID%-GRVariables.yaml
	REM )
	REM if defined Aux2 (
		REM echo AuxField2: %Aux2%>>x:\Windows\System32\bin\Logs\%ID%-GRVariables.yaml
	REM ) else (
		REM echo AuxField2:>>x:\Windows\System32\bin\Logs\%ID%-GRVariables.yaml
	REM )
	REM if defined notes (
		REM echo Notes: %notes%>>x:\Windows\System32\bin\Logs\%ID%-GRVariables.yaml
	REM ) else (
		REM echo Notes:>>x:\Windows\System32\bin\Logs\%ID%-GRVariables.yaml
	REM )
	REM if defined comments (
		REM echo Comments: %comments%>>x:\Windows\System32\bin\Logs\%ID%-GRVariables.yaml
	REM ) else (
		REM echo Comments:>>x:\Windows\System32\bin\Logs\%ID%-GRVariables.yaml
	REM )
	REM echo CosmeticGrade: %CgradeCode%>>x:\Windows\System32\bin\Logs\%ID%-GRVariables.yaml
	REM echo FunctionalGrade: %FgradeCode%>>x:\Windows\System32\bin\Logs\%ID%-GRVariables.yaml
	REM if defined Webcam echo Webcam: %Webcam%>>x:\Windows\System32\bin\Logs\%ID%-GRVariables.yaml
	REM if defined KeyboardLanguage echo KeyboardLanguage: %KeyboardLanguage%>>x:\Windows\System32\bin\Logs\%ID%-GRVariables.yaml
	REM if defined Ssize echo ScreenSize: %Ssize%>>x:\Windows\System32\bin\Logs\%ID%-GRVariables.yaml
	REM if defined Adapter echo ACAdapter: %Adapter%>>x:\Windows\System32\bin\Logs\%ID%-GRVariables.yaml
REM )
echo 6. Return to manage logs.

echo.
choice /c 123456 /n /m "Please select the log file you would like to re-upload."
set choice=!errorlevel!

if !choice!==1 (
	cd x:\Windows\System32\bin
	echo Generating system information log, please wait...
	pcd sysinfo -s %IP% -f>x:\Windows\System32\bin\Logs\!ID!-SYSINFO.yaml
	if !errorlevel! NEQ 0 (
		x:\Windows\System32\JPEGView.exe x:\Windows\System32\Images\Error2.tif
		color c0
		echo PC Doctor failed to generate System Information.
		echo Press any key to retry.&&pause > nul
		Call x:\Windows\System32\bin\Scripts\Function.bat PCDSysInfo
		color
		cls
	)
	for /f "tokens=*" %%f in ('find /I /C "License Authentication Error" !ID!-SYSINFO.yaml') do if %%f==1 Call x:\Windows\System32\bin\Scripts\Function.bat PCDSysInfo
	echo TechnicianUserName: !TechnicianUserName!>>x:\Windows\System32\bin\Logs\!ID!-SYSINFO.yaml
	echo AssetLocation: !Location!>>x:\Windows\System32\bin\Logs\!ID!-SYSINFO.yaml
	if exist x:\Windows\System32\bin\Logs\notes.txt (
		for /F "delims=" %%i in (x:\Windows\System32\bin\Logs\notes.txt) do set notes=!notes! %%i
		echo Notes: !notes!>>x:\Windows\System32\bin\Logs\!ID!-SYSINFO.yaml
	)	
	cd x:\Windows\System32\bin\Logs
	echo Uploading System Information to the Cloud, please wait...
	azcopy /Y /@:"x:\Windows\System32\bin\scripts\azcopyanswer.txt" /pattern:"!ID!-SYSINFO.yaml" > nul 
	if !errorlevel! NEQ 0 (
		x:\Windows\System32\JPEGView.exe x:\Windows\System32\Images\Error3.tif
		color c0
		echo PC Doctor failed to upload System Information.
		echo Press any key to retry upload.&&pause > nul
		Call x:\Windows\System32\bin\Scripts\Function.bat SysInfo
		color
		cls
	) else (
		echo Upload Successful.
		timeout 1 > nul 
		cls
		set sysinfo=Pass&&echo sysinfo:!sysinfo!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		goto UpLogs
	)
)

if !choice!==2 (
	if exist %ID%-SMART.csv (
		echo Uploading SMART results to the Cloud, please wait...
		azcopy /Y /@:"x:\Windows\System32\bin\scripts\azcopyanswer.txt" /pattern:"!ID!-SMART.csv" > nul 
		if !errorlevel! NEQ 0 (
			x:\Windows\System32\JPEGView.exe x:\Windows\System32\Images\Error3.tif
			color c0
			echo PC Doctor failed to upload SMART Results.
			echo Press any key to retry upload.&&pause > nul
			Call x:\Windows\System32\bin\Scripts\Function.bat Smart
			color
			cls
		)
		set SmartUpload=Successful&&echo SmartUpload:!SmartUpload!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		echo Upload Successful.
		timeout 1 > nul 
		goto UpLogs
	) else (
		echo Hard Drive Health Log is unavailable.
		timeout 3 > nul 
		goto UpLogs
	)
)

if !choice!==3 (
	if exist %ID%-WIPE.csv (
		echo Uploading Wipe results to the Cloud, please wait...
		azcopy /Y /@:"x:\Windows\System32\bin\scripts\azcopyanswer.txt" /pattern:"%ID%-WIPE.csv"  > nul
		if !errorlevel! NEQ 0 (
			x:\Windows\System32\JPEGView.exe x:\Windows\System32\Images\Error3.tif
			color c0
			echo PC Doctor failed to upload Wipe Results.
			echo Press any key to retry upload.&&pause > nul
			Call x:\Windows\System32\bin\Scripts\Function.bat Wipe
			color
			cls
		)
		set WipeUpload=Successful&&echo WipeUpload:!WipeUpload!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		echo Upload Successful.
		timeout 1 > nul 
		goto UpLogs
	) else (
		echo Hard Drive Wipe Log is unavailable.
		timeout 3 > nul 
		goto UpLogs
	)
)

if !choice!==4 (
	if exist %ID%-DIAG.csv (
		echo Uploading Diagnostic results to the Cloud, please wait...
		azcopy /Y /@:"x:\Windows\System32\bin\scripts\azcopyanswer.txt" /pattern:"%ID%-DIAG.csv" > nul 
		if !errorlevel! NEQ 0 (
			x:\Windows\System32\JPEGView.exe x:\Windows\System32\Images\Error3.tif
			color c0
			echo PC Doctor failed to upload Diagnostic Results.
			echo Press any key to retry upload.&&pause > nul
			Call x:\Windows\System32\bin\Scripts\Function.bat Diag
			color
			cls
		)
		set DiagUpload=Successful&&echo DiagUpload:!DiagUpload!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		echo Upload Successful.
		timeout 1 > nul 
		goto UpLogs
	) else (
		echo System Diagnostics Log is unavailable.
		timeout 3 > nul 
		goto UpLogs
	)
)

if !choice!==5 (
	call x:\Windows\System32\bin\Scripts\GRVariableGen.bat
	echo Upload Successful.
	timeout 1 > nul 
	goto UpLogs
)

if !choice!==6 (
	goto Logs
)

goto logs
:GRIDLoop
cls

echo Asset Configuration Menu: 
echo What would you like to change? 
echo.
echo 1. Asset ID
echo 2. Inventory Status.
echo 3. Functional Grade.
echo 4. Cosmetic Grade.
echo 5. Auxiliary Fields.
echo 6. Notes.
echo 7. Attribute Values.
echo 8. Return to Main Menu.
choice /c 12345678 /n
set Aconfig=!errorlevel!

if !Aconfig!==1 (
	goto AssetID
)

if !Aconfig!==2 (
	goto InventoryStatus
)

if !Aconfig!==3 (
	goto FunctionalGrade
)

if !Aconfig!==4 (
	goto CosmeticGrade
)

if !Aconfig!==5 (
	goto AuxiliaryField
)

if !Aconfig!==6 (
	goto NotesField
)

if !Aconfig!==7 (
	goto AttributeValues
)

if !Aconfig!==8 (
	echo.
	if exist x:\Windows\System32\bin\Logs\%ID%-GRVariables.yaml (
		call x:\Windows\System32\bin\Scripts\GRVariableGen.bat
	)
	echo Returning to Main Menu.
	timeout 3 > nul 
	goto MenuLoop
)

:AssetID
cls
if defined ID (
	echo Asset ID: %ID%
) else (
	echo This unit's Asset ID is currently undetermined.
)

choice /m "Do you want to update this unit's Asset ID?"

If ERRORLEVEL 2 (
	echo.
	echo Returning to Asset Configuration.
	timeout 3 > nul 
	goto GRIDLoop
)

if ERRORLEVEL 1 (
	echo.
	set IDOld=%ID%
	Call x:\Windows\System32\bin\Scripts\Function.bat AssetID
	for /f "tokens=1,2 delims=:" %%a in (x:\Windows\System32\bin\Logs\LocalVariables.txt) do if %%a==ID set ID=%%b
	if exist x:\Windows\System32\bin\Logs\!IDOld!-SYSINFO.yaml (
		rename x:\Windows\System32\bin\Logs\!IDOld!-SYSINFO.yaml !ID!-SYSINFO.yaml
		echo Uploading System Information to the Cloud, please wait...
		azcopy /Y /@:"x:\Windows\System32\bin\scripts\azcopyanswer.txt" /pattern:"!ID!-SYSINFO.yaml" > nul 
		if !errorlevel! NEQ 0 (
			x:\Windows\System32\JPEGView.exe x:\Windows\System32\Images\Error3.tif
			color c0
			echo PC Doctor failed to upload System Information.
			echo Press any key to retry upload.&&pause > nul
			Call x:\Windows\System32\bin\Scripts\Function.bat SysInfo
			color
			cls
		) else (
			cls
			set sysinfo=Pass&&echo sysinfo:!sysinfo!>>x:\Windows\System32\bin\Logs\LocalVariables.txt&&echo sysinfo:!sysinfo!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		)
	)
	if exist x:\Windows\System32\bin\Logs\!IDOld!-SMART.csv (
		rename x:\Windows\System32\bin\Logs\!IDOld!-SMART.csv !ID!-SMART.csv
		echo Uploading SMART results to the Cloud, please wait...
		azcopy /Y /@:"x:\Windows\System32\bin\scripts\azcopyanswer.txt" /pattern:"!ID!-SMART.csv" > nul 
		if !errorlevel! NEQ 0 (
			x:\Windows\System32\JPEGView.exe x:\Windows\System32\Images\Error3.tif
			color c0
			echo PC Doctor failed to upload SMART Results.
			echo Press any key to retry upload.&&pause > nul
			Call x:\Windows\System32\bin\Scripts\Function.bat Smart
			color
			cls
		)
		set SmartUpload=Successful&&echo SmartUpload:!SmartUpload!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		CLS
	)
	if exist x:\Windows\System32\bin\Logs\!IDOld!-WIPE.csv (
		rename x:\Windows\System32\bin\Logs\!IDOld!-WIPE.csv !ID!-WIPE.csv
		echo Uploading Wipe results to the Cloud, please wait...
		azcopy /Y /@:"x:\Windows\System32\bin\scripts\azcopyanswer.txt" /pattern:"%ID%-WIPE.csv"  > nul
		if !errorlevel! NEQ 0 (
			x:\Windows\System32\JPEGView.exe x:\Windows\System32\Images\Error3.tif
			color c0
			echo PC Doctor failed to upload Wipe Results.
			echo Press any key to retry upload.&&pause > nul
			Call x:\Windows\System32\bin\Scripts\Function.bat Wipe
			color
			cls
		)
		set WipeUpload=Successful&&echo WipeUpload:!WipeUpload!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		cls
	)
	if exist x:\Windows\System32\bin\Logs\!IDOld!-DIAG.csv (
		rename x:\Windows\System32\bin\Logs\!IDOld!-DIAG.csv !ID!-DIAG.csv
		echo Uploading Diagnostic results to the Cloud, please wait...
		azcopy /Y /@:"x:\Windows\System32\bin\scripts\azcopyanswer.txt" /pattern:"%ID%-DIAG.csv" > nul 
		if !errorlevel! NEQ 0 (
			x:\Windows\System32\JPEGView.exe x:\Windows\System32\Images\Error3.tif
			color c0
			echo PC Doctor failed to upload Diagnostic Results.
			echo Press any key to retry upload.&&pause > nul
			Call x:\Windows\System32\bin\Scripts\Function.bat Diag
			color
			cls
		)
		set DiagUpload=Successful&&echo DiagUpload:!DiagUpload!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		cls
	)
	if exist x:\Windows\System32\bin\Logs\!IDOld!-GRVariables.yaml (
		echo Updating information in GRID, please wait...
		rename x:\Windows\System32\bin\Logs\!IDOld!-GRVariables.yaml !ID!-GRVariables.yaml
		azcopy /Y /@:"x:\Windows\System32\bin\scripts\azcopyanswer.txt"  /pattern:"%ID%-GRVariables.yaml" > nul
		if !errorlevel! NEQ 0 (
			x:\Windows\System32\JPEGView.exe x:\Windows\System32\Images\Error3.tif
			color c0
			echo PC Doctor failed to upload GRID values.
			echo Press any key to retry upload.&&pause > nul
			Call x:\Windows\System32\bin\Scripts\Function.bat GRVariables
			color
			cls
		)
		set GRUpload=Successful&&echo GRUpload:!GRUpload!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		cls
	)
	echo.
	echo Asset ID set to !ID!.
	echo Returning to Asset Configuration.
	timeout 3 > nul 
	goto GRIDLoop
)
	
:InventoryStatus
cls
if defined Istatus (
	echo This unit's Inventory Status is currently !Istatus!.
	) else (
		echo This unit's Inventory Status is currently Undetermined.
	)
choice /m "Do you want to change the unit's Inventory Status?"

if ERRORLEVEL 2 (
	echo.
	echo Returning to Asset Configuration.
	timeout 3 > nul 
	goto GRIDLoop
)

if ERRORLEVEL 1 (
	if exist x:\Windows\System32\bin\Logs\%ID%-GRVariables.yaml if defined Istatus set IstatusOld=%Istatus%
	echo Please specify the unit's Inventory Status: 
	echo 1. Test
	echo 2. Wipe
	echo 3. Imaging
	echo 4. Repair
	echo 5. Batching
	echo 6. Retail Ready
	echo 7. FG
	choice /c 1234567 /n
	set Istatus=!errorlevel!

	if !Istatus!==1 (
		set Istatus=Test&&echo Istatus:!Istatus!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		echo.
		echo Inventory Status set to !Istatus!.
		echo Returning to Asset Configuration.
		timeout 3 > nul 
		goto GRIDLoop
	)

	if !Istatus!==2 (
		set Istatus=Wipe&&echo Istatus:!Istatus!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		echo.
		echo Inventory Status set to !Istatus!.
		echo Returning to Asset Configuration.
		timeout 3 > nul 
		goto GRIDLoop
	)

	if !Istatus!==3 (
		set Istatus=Imaging&&echo Istatus:!Istatus!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		echo.
		echo Inventory Status set to !Istatus!.
		echo Returning to Asset Configuration.
		timeout 3 > nul 
		goto GRIDLoop
	)

	if !Istatus!==4 (
		set Istatus=Repair&&echo Istatus:!Istatus!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		echo.
		echo Inventory Status set to !Istatus!.
		echo Returning to Asset Configuration.
		timeout 3 > nul 
		goto GRIDLoop
	)

	if !Istatus!==5 (
		set Istatus=Batching&&echo Istatus:!Istatus!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		echo.
		echo Inventory Status set to !Istatus!.
		echo Returning to Asset Configuration.
		timeout 3 > nul 
		goto GRIDLoop
	)

	if !Istatus!==6 (
		set Istatus=Retail Ready&&echo Istatus:!Istatus!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		echo.
		echo Inventory Status set to !Istatus!.
		echo Returning to Asset Configuration.
		timeout 3 > nul 
		goto GRIDLoop
	)

	if !Istatus!==7 (
		set Istatus=FG&&echo Istatus:!Istatus!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		echo.
		echo Inventory Status set to !Istatus!.
		echo Returning to Asset Configuration.
		timeout 3 > nul 
		goto GRIDLoop
	)
)

:FunctionalGrade
cls
if defined Fgrade (
	echo This unit's Functional Grade is currently !Fgrade!.
	) else (
		echo This unit's Functional Grade is currently Undetermined.
		)
choice /m "Do you want to change the unit's Functional Grade?"

if ERRORLEVEL 2 (
	echo.
	echo Returning to Asset Configuration.
	timeout 3 > nul 
	goto GRIDLoop
)

if ERRORLEVEL 1 (
	if exist x:\Windows\System32\bin\Logs\%ID%-GRVariables.yaml if defined Fgrade set FgradeOld=%Fgrade%
	echo Please specify the unit's Functional Grade: 
	echo 1. Functional
	echo 2. Functional-Incomplete
	echo 3. Non-Functional
	choice /c 123 /n
	set Fgrade=!errorlevel!

	if !Fgrade!==1 (
		set Fgrade=Functional&&echo Fgrade:!Fgrade!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		set Incomplete=False&&echo Incomplete:!Incomplete!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		echo.
		echo Functional Grade set to !Fgrade!.
		echo Returning to Asset Configuration.
		timeout 3 > nul 
		goto GRIDLoop
	)

	if !Fgrade!==2 (
		set Fgrade=Functional-Incomplete&&echo Fgrade:!Fgrade!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		set Incomplete=True&&echo Incomplete:!Incomplete!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		echo.
		echo Functional Grade set to !Fgrade!.
		echo Returning to Asset Configuration.
		timeout 3 > nul 
		goto GRIDLoop
	)

	if !Fgrade!==3 (
		set Fgrade=Non-Functional&&echo Fgrade:!Fgrade!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		echo.
		echo Functional Grade set to !Fgrade!.
		echo Returning to Asset Configuration.
		timeout 3 > nul 
		goto GRIDLoop
	)
)

:CosmeticGrade
cls
if defined Cgrade (
	echo This unit's Cosmetic Grade is currently !Cgrade!.
	) else (
		echo This unit's Cosmetic Grade is currently Undetermined.
		)
choice /m "Do you want to change the unit's Cosmetic Grade?"

if ERRORLEVEL 2 (
	echo.
	echo Returning to Asset Configuration.
	timeout 3 > nul 
	goto GRIDLoop
)

if ERRORLEVEL 1 (
	if exist x:\Windows\System32\bin\Logs\%ID%-GRVariables.yaml if defined Cgrade set CgradeOld=%Cgrade%
	echo Please specify the unit's Cosmetic Grade: 
	echo 1. A
	echo 2. B
	echo 3. C
	echo 4. D
	choice /c 1234 /n
	set Cgrade=!errorlevel!

	if !Cgrade!==1 (
		set Cgrade=A&&echo Cgrade:!Cgrade!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		echo.
		echo Cosmetic Grade set to !Cgrade!.
		echo Returning to Asset Configuration.
		timeout 3 > nul 
		goto GRIDLoop
	)

	if !Cgrade!==2 (
		set Cgrade=B&&echo Cgrade:!Cgrade!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		echo.
		echo Cosmetic Grade set to !Cgrade!.
		echo Returning to Asset Configuration.
		timeout 3 > nul 
		goto GRIDLoop
	)

	if !Cgrade!==3 (
		set Cgrade=C&&echo Cgrade:!Cgrade!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		echo.
		echo Cosmetic Grade set to !Cgrade!.
		echo Returning to Asset Configuration.
		timeout 3 > nul 
		goto GRIDLoop
	)

	if !Cgrade!==4 (
		set Cgrade=D&&echo Cgrade:!Cgrade!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		echo.
		echo Cosmetic Grade set to !Cgrade!.
		echo Returning to Asset Configuration.
		timeout 3 > nul 
		goto GRIDLoop
	)
)

:AuxiliaryField
cls
if exist x:\Windows\System32\bin\Logs\%ID%-GRVariables.yaml if defined aux1 set aux1Old=%aux1%
if defined aux1 (
	echo Auxiliary Field 1: !aux1!
	) else (
		echo Auxiliary Field 1 is empty.
		)
choice /m "Do you want to add data to Auxiliary field 1?"

if ERRORLEVEL 1 (
	echo.
	set /p aux1=Please enter data to be added to Auxiliary field 1: 
	echo.&&echo aux1:!aux1!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
	echo Auxiliary Field 1: !aux1!.
	echo.
)
if exist x:\Windows\System32\bin\Logs\%ID%-GRVariables.yaml if defined aux2 set aux2Old=%aux2%
if defined aux2 (
	echo Auxiliary Field 2: !aux2!
	) else (
		echo Auxiliary Field 2 is empty.
		)
choice /m "Do you want to add data to Auxiliary field 2?"
if not ERRORLEVEL 2 (
	echo.
	set /p aux2=Please enter data to be added to Auxiliary field 2: 
	echo.&&echo aux2:!aux2!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
	echo Auxiliary Field 2: !aux2!.
	echo Returning to Asset Configuration.
	timeout 3 > nul 
	goto GRIDLoop
) else (
	echo.
	echo Returning to Asset Configuration.
	timeout 3 > nul 
	goto GRIDLoop
)

:NotesField
cls
if exist x:\Windows\System32\bin\Logs\%ID%-GRVariables.yaml if defined notes set notesOld=%notes%
cd x:\Windows\System32\bin\Logs
if exist notes.txt (
	set notes=
	for /F "delims=" %%i in (notes.txt) do set notes=!notes! %%i
)
if defined notes (
	echo Notes:!notes!
	) else (
		echo The Notes Field is empty.
		)
choice /m "Do you want to add data to the Notes field?"

if ERRORLEVEL 2 (
	choice /m "Do you want to clear the Notes Field?"
	if ERRORLEVEL 2 (
		echo Returning to Asset Configuration.
		timeout 3 > nul 
		goto GRIDLoop
	) else (
		del notes.txt=
		set notes=
		echo Deleting Notes&&echo notes:!notes!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		timeout 3 > nul 
		goto GRIDLoop
	)
)		


if ERRORLEVEL 1 (
	set /p note=Please enter data to be added to the Notes field: 
	echo !note!>>notes.txt
	set notes=
	for /F "delims=" %%i in (notes.txt) do set notes=!notes! %%i
	echo.&&echo notes:!notes!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
	echo Notes:!notes!
	echo Returning to Asset Configuration.
	timeout 3 > nul 
	goto GRIDLoop
)

:AttributeValues
cls
if defined Adapter (
	echo This unit's AC Adapter is currently !Adapter!.
	) else (
		echo This unit's AC Adapter status is currently Undetermined.
	)
if defined battery (
	echo This unit's Battery is currently !battery!.
	) else (
		echo This unit's Battery status is currently Undetermined.
	)
if defined Webcam (
	echo This unit's Webcam is currently !Webcam!.
	) else (
		echo This unit's Webcam status is currently Undetermined.
	)
if defined Ssize (
	echo This unit's Screen Size is currently !Ssize!.
	) else (
		echo This unit's Screen Size is currently Undetermined.
	)
echo.
choice /m "Do you want to change the unit's Attribute Values?"

if ERRORLEVEL 2 (
	echo.
	if exist x:\Windows\System32\bin\Logs\%ID%-GRVariables.yaml (
		if defined AdapterOld (				
			powershell -Command "(gc x:\Windows\System32\bin\Logs\%ID%-GRVariables.yaml) -replace 'ACAdapter: %AdapterOld%', 'ACAdapter: %Adapter%' | Out-File x:\Windows\System32\bin\Logs\%ID%-GRVariables.yaml"
			set AdapterOld=
		)
		if defined WebcamOld (
			powershell -Command "(gc x:\Windows\System32\bin\Logs\%ID%-GRVariables.yaml) -replace 'Webcam: %WebcamOld%', 'Webcam: %Webcam%' | Out-File x:\Windows\System32\bin\Logs\%ID%-GRVariables.yaml"
			set WebcamOld=
		)
		if defined SsizeOld (
			powershell -Command "(gc x:\Windows\System32\bin\Logs\%ID%-GRVariables.yaml) -replace 'ScreenSize: %SsizeOld%', 'ScreenSize: %Ssize%' | Out-File x:\Windows\System32\bin\Logs\%ID%-GRVariables.yaml"
			set SsizeOld=
		)
	)
	echo Returning to Asset Configuration.
	timeout 3 > nul 
	goto GRIDLoop
)

if ERRORLEVEL 1 (
	echo. 
	echo Which attribute would you like to change?
	echo 1. AC Adapter
	echo 2. Battery
	echo 3. Webcam
	echo 4. Screen Size
	echo 5. Return to Asset Configuration
	choice /c 12345 /n
	set Avalue=!errorlevel!

	if !Avalue!==1 (
		goto Adapter
	)

	if !Avalue!==2 (
		goto Battery
	)

	if !Avalue!==3 (
		goto Webcam
	)

	if !Avalue!==4 (
		goto ScreenSize
	)

	if !Avalue!==5 (
		goto GRIDLoop
	)
)


:Adapter
cls
if exist x:\Windows\System32\bin\Logs\%ID%-GRVariables.yaml if defined Adapter set AdapterOld=%Adapter%
choice /m "Does this unit include an AC Adapter?"
if ERRORLEVEL 2 (
	set Adapter=Not-Installed&&echo Adapter:!Adapter!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
	set Incomplete=True&&echo Incomplete:!Incomplete!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
) else (
	set Adapter=Installed&&echo Adapter:!Adapter!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
)
echo.
echo This unit's AC Adapter is !Adapter!.
echo Returning to Attribute Values.
timeout 3 > nul 
goto AttributeValues

:Battery
cls
REM if defined battery set batteryOld=%battery%
choice /m "Does this unit include a battery?"
if ERRORLEVEL 2 (
	set Incomplete=True&&echo Incomplete:!Incomplete!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
	set battery=Not-Installed&&echo battery:!battery!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
) else (
	set battery=Installed&&echo battery:!battery!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
)
echo.
echo This unit's Battery is !battery!.
echo Returning to Attribute Values.
timeout 3 > nul 
goto AttributeValues

:Webcam 
cls
if exist x:\Windows\System32\bin\Logs\%ID%-GRVariables.yaml if defined Webcam set WebcamOld=%Webcam%
choice /m "Does this unit include a Webcam?"
if ERRORLEVEL 2 (
	set Webcam=Not-Installed&&echo Webcam:!Webcam!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
) else (
	set Webcam=Installed&&echo Webcam:!Webcam!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
)
echo.
echo This unit's Webcam is !Webcam!.
echo Returning to Attribute Values.
timeout 3 > nul 
goto AttributeValues
:ScreenSize
cls
if exist x:\Windows\System32\bin\Logs\%ID%-GRVariables.yaml if defined Ssize set SsizeOld=%Ssize%
set tempvar=0
echo Please specify the unit's Screen Size: 
set /p Ssize=

if !Ssize! GEQ 10 (
	if !Ssize! LEQ 23 (
		set tempvar=1
		set Ssize=!Ssize!
		echo.&&echo Ssize:!Ssize!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
		echo This unit's Screen Size is set to !Ssize!.
		echo Returning to Attribute Values
		timeout 3 > nul 
		goto AttributeValues
	)
)

if NOT !tempvar!==1 (
	echo You have specified an invalid screen size!
	timeout 3 > nul 
	goto ScreenSize
)

:EOF