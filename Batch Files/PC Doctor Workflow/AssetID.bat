@echo off
setlocal enabledelayedexpansion
:AssetID
set /p ID= Scan Asset ID: 
set ID=!ID: =!
ECHO !ID!>len.txt
FOR %%? IN (len.txt) DO ( SET /A strlength=%%~z? - 2 )
if not !strlength!==10 (
	echo Invalid Asset ID, please re-scan
	goto AssetID
)
if /i %ID:~0,2% NEQ US (
	if /i %ID:~0,2% NEQ UK (
		echo Invalid Asset ID, please re-scan
		goto AssetID
	)
)
echo ID:!ID!>>x:\Windows\System32\bin\Logs\LocalVariables.txt
echo ID:!ID!>>x:\Windows\System32\bin\Logs\input.txt
:EOF